import {Collections } from '/collections'
import { Publication } from '/stereor'

const defaultLimit = 500

export const magic2 = new Publication(function(queries = []) {
	const cursors = []

	queries.forEach(q => {
		const collection = Collections[q[0]]
		const queryDefault = q[1] || {}
		const options = q[2] || {
			sort: {
				createdAt: -1,
			},
			limit: defaultLimit,
		}
		//q[3] - admin mode if true
		if (!collection) {
			throw new Error(`${q[0]} collection not exists`)
		}
		const query = {
			...queryDefault,
		}
		if (!Meteor.settings.admin) {
			query.$or = [
				{ _user: Meteor.userId() },
				{ _user: { $exists: false } },
			]
		}
		cursors.push(collection.find(query, options))
	})
	return cursors
})