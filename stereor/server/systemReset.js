import { Collections } from '/collections'

export const systemReset = () => {
	Object.keys(Collections).forEach(name => {
		Collections[name].remove({})
	})
}