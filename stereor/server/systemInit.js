import { initialization } from '/data/initialization'
import * as cols from '/collections'

export const systemInit = () => {
	console.warn('system initialization')
	Object.keys(initialization).forEach(name => {
		initialization[name].forEach(item => {
			console.log(name, item._id, 'initialized')
			if (item._id) {
				cols[name].upsert(item._id, { $set: item })
			} else {
				cols[name].insert(item)
			}
		})
	})
}

