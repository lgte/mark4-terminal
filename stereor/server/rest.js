import { Meteor } from 'meteor/meteor'

import * as publications from '/app/api/publications'
import * as methods from '/app/api/methods'

const QueryString = require('querystring')

const publicationRoutes = Picker.filter(function(req, res) {
	return req.method == 'GET'
})

const methodRoutes = Picker.filter(function(req, res) {
	return req.method == 'POST'
})

const optionsRoutes = Picker.filter(function(req, res) {
	if (req.method === 'OPTIONS') {
		res.setHeader('Access-Control-Allow-Origin', '*')
		res.setHeader('Content-Type', 'application/json')
		res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
		res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
	}

	return req.method == 'OPTIONS'
})

Object.keys(publications).forEach(name => {
	//console.log('get', `/api/${name}`)
	publicationRoutes.route(`/api/${name}`, function(params, req, res, next) {

		res.setHeader('Access-Control-Allow-Origin', '*')
		res.setHeader('Content-Type', 'application/json')
		res.setHeader('Access-Control-Allow-Methods', 'GET, OPTIONS')
		res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
		res.writeHead(200)
		const func = publications[name].func
		const data = QueryString.parse(req._parsedUrl.query)
		const result = Meteor.bindEnvironment(func)(data)

		res.end(JSON.stringify(result.fetch()))
	})
})

Object.keys(methods).forEach(name => {
	//console.log('post', `/api/${name}`)
	methodRoutes.route(`/api/${name}`, function(params, req, res, next) {
		res.setHeader('Access-Control-Allow-Origin', '*')
		res.setHeader('Content-Type', 'application/json')
		res.setHeader('Access-Control-Allow-Methods', 'POST, OPTIONS, PUT, PATCH, DELETE')
		res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')

		let body = ''
		req.on('readable', function() {
			body += req.read()
		})
		req.on('end', async function() {
			const json = body.substr(0, body.length - 4).trim()
			const data = JSON.parse(json)

			try {
				//console.log(`/api/${name}`)
				const result = Meteor.call(name, data)
				res.writeHead(200)
				res.end(JSON.stringify(result))
			} catch (e) {
				console.error(e)
				res.writeHead(400)
				res.end(JSON.stringify({ error: e.message }))
			}
		})
	})
})