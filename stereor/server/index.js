import * as publications from '/publications'
import * as methods from '/methods'
import { addPublications, addMethods } from '/stereor'

addPublications(publications)
addMethods(methods)

import '/server.js'