import React, { useState } from 'react'
import { Meteor } from 'meteor/meteor'
import { withTracker } from 'meteor/react-meteor-data'
import { Tracker } from 'meteor/tracker'
import { Constants } from '/constants'

import _ from 'lodash'

export const Stereor = {
	handlerError: console.error,
	handlerSuccess: () => '',
}

export * from './Collection'

export const stereorSettings = Object.assign({
	defaultErrorHandler(err, method) {
		console.log(err)
		clientUserId()
			? call('notificationInsert', {
				type: 'error',
				title: 'Error',
				description: err.reason || err.message,
			})
			: console.error(err.reason || err.message)
	},
	defaultSuccessHandler(res, method) {
		if (method === 'notificationInsert') {
			return
		}
		if (method === 'notificationMarkAsShown') {
			return
		}
		clientUserId() && method
			? call('notificationInsert', {
				type: 'message',
				title: 'Success',
				description: method,
			})
			: '' //console.log(method || 'operation', 'success')
	},
}, Meteor.settings.stereor || {})

export const callbackHandler = (err, res) => {
	console.log(err, res)
	err
		? stereorSettings.defaultErrorHandler(err)
		: stereorSettings.defaultSuccessHandler(res)
}

export const getWithTracker = withTracker

export class Method {
	constructor(func, schema = {}, opts = {}) {
		this.func = func
		this.schema = schema
		this.opts = opts
	}
}

export class Publication {
	constructor(func, schema = {}, opts = {}) {
		this.func = func
		this.schema = schema
		this.opts = opts
	}
}

export const connect = url => {
	return new Promise((resolve, reject) => {
		const connection = DDP.connect(url)
		Tracker.autorun(computation => {
			if (connection.status().connected) {
				computation.stop()
				resolve(connection)
			}
			if (connection.status().connected === 'failed') {
				reject()
			}
		})
	})
}

export const getForm = (defaultValues = {}) => {
	const [values, setValues] = useState(defaultValues)
	const getInputAttributes = (name) => {
		!values[name] && (values[name] = '')
		return {
			onChange(e) {
				setValues({
					...values,
					[e.target.name]: e.target.type === 'number'
						? +e.target.value
						: e.target.value,
				})
			},
			name,
			value: values[name],
		}
	}
	const getFormAttributes = (onSubmit) => {
		return {
			onSubmit(e) {
				e.preventDefault()
				e.target.value = values
				onSubmit(e)
			},
		}
	}
	return {
		values,
		setValues,
		getInputAttributes,
		getFormAttributes,
	}
}

export const mapItems = (data, Com, payload = {}) => {
	check(data, Array)
	return data.map((item, key) => <Com item={item} payload={payload} key={key}/>)
}

export const getObjectId = () => new Mongo.ObjectID()._str

export const subscribe = (...args) => new Promise((resolve, reject) => {
	args.push((err) => {
		err ? reject(err) : resolve()
	})
	Meteor.isClient && Meteor.subscribe.apply(null, args)
})

// То же самое что Meteor.call только возвращает promise
// Если первый аргумент connection то метод будет вызван через это соединение
export const call = (...args) => new Promise((resolve, reject) => {
	const connection = _.isObject(args[0]) ? args.shift() : Meteor
	const method = args.shift()
	connection.apply(method, args, (err, res) => {
		err ? Stereor.handlerError(err) : Stereor.handlerSuccess(method, res)
		err ? reject(err) : resolve(res)
	})
})

export const addPublications = (publications) => {
	Object.keys(publications).forEach(name => {
		Meteor.publish(name, publications[name].func)
	})
}

export const addMethods = (methods) => {
	if (methods.default) {
		return
	}
	const names = Object.keys(methods)
	names.forEach(name => {
		Meteor.methods({
			[name]: methods[name].func,
		})
	})
}

export const importData = (collection, data = [], mutator = element => element) => {
	if (data.length === 0) {
		return
	}
	if (data[0] && !data[0]._id) {
		collection.remove({})
	}
	data.forEach(record => collection.upsert(
		{ _id: record._id },
		{ $set: mutator(record) },
	))
}

export const usersInsertAnonymous = (method) => new Promise(async (resolve, reject) => {
	Tracker.autorun(function(computation) {
		if (Meteor.user() === null) {
			const token = getObjectId()
			const profile = {
				name: 'Anonymous',
				anonymous: true,
			}
			if (method) {
				call(method, { username: token, password: token, profile })
					.then(_user => {
						Meteor.loginWithPassword(token, token)
					})
					.catch(e => {
						console.error(e)
						computation.stop()
						reject(e)
					})
			} else {
				Accounts.createUser({
					username: token,
					password: token,
					profile,
				}, (err, res) => {
					if (err) {
						console.error(e)
						computation.stop()
						reject(e)
					} else {
						console.log('userCreated', res)
					}
				})
			}

		} else if (Meteor.user()) {
			computation.stop()
			call('logsInsert', {
				group: Constants.LOG_GROUP.USER,
				level: 'message',
				message: 'User logged in' + Meteor.userId()
			})
			resolve(Meteor.userId())
		}
	})
})

export const clientUserId = () => Meteor.isClient ? Meteor.userId() : null

export const clientUser = () => Meteor.isClient ? Meteor.user() : null

export const flatten = (val, result = {}, key = '') => {

	if (_.isArray(val)) {
		val.forEach((item, i) => {
			Object.assign(result, flatten(item, result, `${key}${i}.`))
			return result
		})
	}

	if (_.isObject(val)) {
		Object.keys(val).forEach(k => {
			Object.assign(result, flatten(val[k], result, `${key}${k}.`))
			return result
		})
	}

	if (key !== '') {
		return { [key.substr(0, key.length - 1)]: val }
	}

	return result
}