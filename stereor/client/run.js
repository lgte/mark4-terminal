import React from 'react'
import { render } from 'react-dom'

export const run = App => {
	render(<App/>, document.getElementById('app'))
}