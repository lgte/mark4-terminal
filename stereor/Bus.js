export const Bus = {
	subscriptions: {},

	pub(event, payload) {
		check(event, String)
		this.subscriptions[event] || (this.subscriptions[event] = [])
		this.subscriptions[event].forEach(el => {
			el(payload)
		})
	},

	sub(event, handler) {
		check(event, String)
		this.subscriptions[event] || (this.subscriptions[event] = [])
		this.subscriptions[event].push(handler)
	}
}