const optionsGlobal = {
	sort: { }
}

export const Memory = {}

export class Collection extends Mongo.Collection {

	schema = null
	name = null

	constructor(name, schema = {}, options = {}) {
		super(name)
		this.name = name
		this.schema = schema
		this.options = Object.assign({
			inMemory: false
		}, options)
		this.inMemory = Meteor.isServer && this.options.inMemory

		schema === false || this.attachSchema(schema)

		Memory[name] = {}

		this.inMemory && this.find().observe({
			added: async doc => Memory[name][doc._id] = doc,
		})

	}

	find(selector = {}, optionsLocal = {}) {
		const options = Object.assign({}, optionsGlobal, optionsLocal)
		return super.find(selector, options)
	}

	findOrDie(selector = {}, optionsLocal = {}, count) {
		check(count, Number)
		const cursor = this.find(selector, optionsLocal)
		if (count && cursor.count() !== count) {
			console.error('findOrDie', this.name, selector)
			console.error('findOrDie', this.name, cursor.fetch())
			console.error('findOrDie', {countExpected: count, countReceived: cursor.count() })
			throw new Error(`can't fetch required result `)
		}
		return cursor
	}

	findOneOrDie(...args) {
		const result = this.findOne.apply(this, args)
		if (!result) {
			console.error('findOneOrDie', this.name, args)
			throw new Error(`can't fetch required result count`)
		}
		return result
	}

	findByIdInMemory(_id) {
		return Memory[this.name][_id] || this.findOrDie(_id)
	}

}