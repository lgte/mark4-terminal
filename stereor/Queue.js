let resolveToStart = undefined
const defaultErrorHandler = e => {
	console.error('error in queue')
	console.error(e)
}

const defaultScuccessHandler = res => {
	//doing nothing by default
}

export const Queue = {

	resolve: undefined,
	started: false,
	verbose: false,
	count: 0,

	recentPromise: new Promise((resolve) => {
		resolveToStart = resolve
	}),

	push(asyncFunc, name, handlerError = defaultErrorHandler, handlerSuccess = defaultScuccessHandler) {
		if (!name) {
			throw new Error(`Task name is empty`)
		}

		this.recentPromise = this
			.recentPromise
			.then(asyncFunc)
			.then(handlerSuccess)
			.catch(handlerError)

		return this
	},

	createTask(asyncFunc, name, handlerError, handlerSuccess) {
		return (...args) => {
			this.push(() => asyncFunc.apply(null, args), name, handlerError, handlerSuccess)
		}
	},

	run(verbose = false) {
		this.started = true
		this.verbose = verbose
		resolveToStart()
	},

}
