


export const Tokens = [
	{ _id: 'btc', title: 'Bitcoin', symbol: 'BTC' },
	{ _id: 'eth', title: 'Etherium', symbol: 'ETH' },
	{ _id: 'etc', title: 'Etherium Classic', symbol: 'ETC' },
	{ _id: 'ltc', title: 'Litecoin', symbol: 'LTC' },
	{ _id: 'eos', title: 'EOS', symbol: 'EOS' },
	{ _id: 'xrp', title: 'XRP', symbol: 'XRP' },
	{ _id: 'bch', title: 'Bitcoin Cache', symbol: 'BCH' },
	{ _id: 'trx', title: 'TRON', symbol: 'TRX' },
	{ _id: 'xlm', title: 'Stellar', symbol: 'XLM' },
	{ _id: 'bsv', title: 'Bitcoin SV', symbol: 'BSV' },
	{ _id: 'usdt', title: 'Tether', symbol: 'USDT' },
	{ _id: 'ada', title: 'Cardano', symbol: 'ADA' },
	{ _id: 'xmr', title: 'Monero', symbol: 'XMR' },
	{ _id: 'dash', title: 'Dash', symbol: 'DASH' },
	{ _id: 'iota', title: 'IOTA', symbol: 'IOTA' },
	{ _id: 'nem', title: 'Nem', symbol: 'NEM' },
	{ _id: 'zec', title: 'Zec', symbol: 'ZEC' },
	{ _id: 'ada', title: 'Ada', symbol: 'ADA' },
	{ _id: 'neo', title: 'Neo', symbol: 'NEO' },
	{ _id: 'xem', title: 'Xem', symbol: 'XEM' },
	{ _id: 'qtum', title: 'Qtum', symbol: 'QTUM' },
]