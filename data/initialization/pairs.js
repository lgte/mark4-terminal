/**
LTC/BTC
EOS/BTC
ETH/BTC
XRP/BTC
BCHABC/BTC
TRX/BTC
XLM/BTC
ETC/BTC
XMR/BTC
BCHSV/BTC
DASH/BTC
QTUM/BTC
*/

export const Pairs = [
	//BTC Base
	{ _id: 'ethbtc', _tokenBase: 'btc', _tokenQuoted: 'eth', price: 0 },
	{ _id: 'bchbtc', _tokenBase: 'btc', _tokenQuoted: 'bch', price: 0 },
	{ _id: 'ltcbtc', _tokenBase: 'btc', _tokenQuoted: 'ltc', price: 0 },
	{ _id: 'xrpbtc', _tokenBase: 'btc', _tokenQuoted: 'xrp', price: 0 },
	{ _id: 'eosbtc', _tokenBase: 'btc', _tokenQuoted: 'eos', price: 0 },
	{ _id: 'xlmbtc', _tokenBase: 'btc', _tokenQuoted: 'xlm', price: 0 },
	{ _id: 'etcbtc', _tokenBase: 'btc', _tokenQuoted: 'etc', price: 0 },
	{ _id: 'zecbtc', _tokenBase: 'btc', _tokenQuoted: 'zec', price: 0 },
	{ _id: 'adabtc', _tokenBase: 'btc', _tokenQuoted: 'ada', price: 0 },
	{ _id: 'neobtc', _tokenBase: 'btc', _tokenQuoted: 'neo', price: 0 },
	{ _id: 'xembtc', _tokenBase: 'btc', _tokenQuoted: 'xem', price: 0 },

	//ETH Base
	{ _id: 'bcheth', _tokenBase: 'eth', _tokenQuoted: 'bch', price: 0 },
	{ _id: 'ltceth', _tokenBase: 'eth', _tokenQuoted: 'ltc', price: 0 },
	{ _id: 'xrpeth', _tokenBase: 'eth', _tokenQuoted: 'xrp', price: 0 },

	//usdt
	{ _id: 'btcusd', _tokenBase: 'usdt', _tokenQuoted: 'btc', price: 0 },
	{ _id: 'ethusd', _tokenBase: 'usdt', _tokenQuoted: 'eth', price: 0 },
]

// -
// BCHABC/BTC
