export const Users = [
	{ _id: 'root', createdAt: new Date() },

	{ _id: 'issuer', createdAt: new Date() },
	{ _id: 'banker', createdAt: new Date() },
	{ _id: 'custodian', createdAt: new Date() },
	{ _id: 'beneficiary', createdAt: new Date() },

	// auditors
	{ _id: 'auditor-commission-maker', createdAt: new Date() },
	{ _id: 'auditor-commission-taker', createdAt: new Date() },
	{ _id: 'auditor-commission-deposit', createdAt: new Date() },
	{ _id: 'auditor-commission-withdraw', createdAt: new Date() },
	{ _id: 'auditorRobots', createdAt: new Date() },

	{ _id: 'auditorWithdraw', createdAt: new Date() },
	{ _id: 'auditorDeposit', createdAt: new Date() },

	// adapters
	{ _id: 'adapterInManual', createdAt: new Date() },

	{ _id: 'adapterInBtc', createdAt: new Date() },
	{ _id: 'adapterInEth', createdAt: new Date() },
	{ _id: 'adapterInLtc', createdAt: new Date() },
	{ _id: 'adapterInUsdt', createdAt: new Date() },

	{ _id: 'adapterOutBtc', createdAt: new Date() },
	{ _id: 'adapterOutEth', createdAt: new Date() },
	{ _id: 'adapterOutLtc', createdAt: new Date() },
	{ _id: 'adapterOutUsdt', createdAt: new Date() },
]