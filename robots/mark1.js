import _ from 'lodash'

import { call, connect } from '/stereor'
import { Constants } from '/constants'
import { login } from './tools/login'

export class Mark1 {

	ORDERS_MAX = 15

	constructor(params) {

		check(params.username, String)
		check(params.password, String)
		check(params.server, String)
		check(params._pair, String)

		this.params = Object.assign(
			{
				interval: 10000,
				onStarted: () => '',
			},
			params,
		)

		this.start()
	}

	async start() {
		const connection = await connect(this.params.server)
		const user = {
			username: this.params.username,
			password: this.params.password,
			code: '1342567890iopkljbvncxzfdgehjmyh',
		}
		await login(connection, user)
		Meteor.setInterval(() => { this.work(connection) }, this.params.interval)
		this.work(connection)

	}

	async work(connection) {

		const ordersActive = await call(connection, 'ordersFindActive', { _pair: this.params._pair })
		const ordersBuy = ordersActive.filter(el => el.direction === Constants.ORDER_DIRECTIONS.BUY)
		const ordersSell = ordersActive.filter(el => el.direction === Constants.ORDER_DIRECTIONS.SELL)
		const synchronization = await call(connection, 'synchronizationsFindRecent')
		const ticker = synchronization.payload.find(i => i.symbol === this.params._pair.toUpperCase())

		if (!ticker) {
			console.warn(this.params.username, 'no synchronization data, skipping move')
			return
		}

		/**
		console.table({
			maxOrder: this.ORDERS_MAX,
			ordersSellCount: ordersSell.length,
			ordersBuyCount: ordersBuy.length
		})
		 */

		if (ordersBuy.length < this.ORDERS_MAX) {
			call(connection, 'ordersInsert', {
				_pair: this.params._pair,
				amountBase: _.random(0.01, 1, true),
				price: ticker.ask * _.random(0.9, 1.1, true),
				direction: Constants.ORDER_DIRECTIONS.SELL,
				type: Constants.ORDER_TYPES.STOP,
				orderTag: this.params.username,
			})
		}

		if (ordersSell.length < this.ORDERS_MAX) {
			call(connection, 'ordersInsert', {
				_pair: this.params._pair,
				amountBase: _.random(0.01, 1, true),
				price: ticker.ask * _.random(0.9, 1.1, true),
				direction: Constants.ORDER_DIRECTIONS.BUY,
				type: Constants.ORDER_TYPES.STOP,
				orderTag: this.params.username,
			})
		}
	}

}
