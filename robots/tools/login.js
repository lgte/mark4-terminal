import { call } from '/stereor'

export const login = async (connection, user) => {
	await call(connection, 'usersInsertOrLogin', user)
	const result = await call(connection, 'login', {
		'user': {
			'username': user.username,
		},
		'password': user.password,
	})
	return result
}