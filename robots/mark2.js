import _ from 'lodash'

import { call, connect } from '/stereor'
import { Constants } from '/constants'
import { login } from './tools/login'

export class Mark2 {

	constructor(params) {

		check(params.username, String)
		check(params.password, String)
		check(params.server, String)
		check(params._pair, String)

		this.params = Object.assign(
			{
				interval: 10000,
				onStarted: () => '',
			},
			params,
		)

		this.start()

	}

	async start() {
		const connection = await connect(this.params.server)
		const user = {
			username: this.params.username,
			password: this.params.password,
			code: '1342567890iopkljbvncxzfdgehjmyh',
		}
		await login(connection, user)
		Meteor.setInterval(() => { this.worker(connection) }, this.params.interval)

	}

	async worker(connection) {

		const ordersActive = await call(connection, 'ordersFindActive', { _pair: this.params._pair })
		const orderBuy = ordersActive.find(el => el.direction === Constants.ORDER_DIRECTIONS.BUY)
		const orderSell = ordersActive.find(el => el.direction === Constants.ORDER_DIRECTIONS.SELL)
		const synchronization = await call(connection, 'synchronizationsFindRecent')
		const ticker = synchronization.payload.find(i => i.symbol === this.params._pair.toUpperCase())

		if (!ticker) {
			console.warn(this.params.username, 'no synchronization data, skipping move')
			return
		}

		if (orderBuy) {
			call(connection, 'ordersInsert', {
				_pair: this.params._pair,
				amountBase: orderBuy.amountBase * _.random(0.1, 0.5, true),
				direction: Constants.ORDER_DIRECTIONS.SELL,
				type: Constants.ORDER_TYPES.MARKET,
				orderTag: this.params.username,
			})
		}

		if (orderSell) {
			call(connection, 'ordersInsert', {
				_pair: this.params._pair,
				amountBase: orderSell.amountBase * _.random(0.1, 0.5, true),
				direction: Constants.ORDER_DIRECTIONS.BUY,
				type: Constants.ORDER_TYPES.MARKET,
				orderTag: this.params.username,
			})
		}

	}

}
