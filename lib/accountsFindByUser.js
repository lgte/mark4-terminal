import { Lib } from '/lib'
import { Constants } from '/constants'
import { Collections } from '/collections'

export const accountsFindByUser = (_user, _token) => {
	check(_user, String)
	check(_token, String)
	return Collections.Accounts.findOneOrDie({_user, _token})
}