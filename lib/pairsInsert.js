import { Pairs } from '/collections'
import { Collections } from '/collections'

export const pairsInsert = data => {
	check(data._id, String)
	check(data._tokenBase, String)
	check(data._tokenQuoted, String)

	Collections.Tokens.findOneOrDie(data._tokenBase)
	Collections.Tokens.findOneOrDie(data._tokenQuoted)

	return Pairs.upsert(
		{ _id: data._id },
		{ $set: data },
	)
}