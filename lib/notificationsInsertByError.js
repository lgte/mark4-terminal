import { Constants } from '/constants'
import { Collections } from '/collections'
import { notificationsInsert } from './notificationsInsert'

export const notificationsInsertByError = (e) => {
	console.error(e)
	return notificationsInsert({
		_user: Meteor.userId(),
		title: e.message,
		type: 'error'
	})
}