import { Params } from '/collections'

export const paramsSet = (_id, value) => {
	Params.upsert(_id, { $set: {_id, value} })
	return value
}