import { Collections } from '/collections'
import { Constants } from '/constants'
import { transactionInsertUser2User } from './transactionInsertUser2User'

export const tradesInsert = trade => {

	const orderSell = Collections.Orders.findOneOrDie(trade._orderSell)
	const orderBuy = Collections.Orders.findOneOrDie(trade._orderBuy)
	const pair = Collections.Pairs.findOneOrDie(orderSell._pair)

	const commissionSell = trade.amountQuoted * (orderSell.type === Constants.ORDER_TYPES.MARKET
			? Constants.SETTINGS_GLOBAL.COMMISSION_TAKER
			: Constants.SETTINGS_GLOBAL.COMMISSION_MAKER
	)
	const commissionBuy = trade.amountBase * (orderBuy.type === Constants.ORDER_TYPES.MARKET
			? Constants.SETTINGS_GLOBAL.COMMISSION_TAKER
			: Constants.SETTINGS_GLOBAL.COMMISSION_MAKER
	)

	transactionInsertUser2User({
		_userCredit: Constants.USERS.CUSTODIAN,
		_userDebit: orderSell._user,
		_token: pair._tokenQuoted,
		amount: trade.amountQuoted - commissionSell,
	})

	transactionInsertUser2User({
		_userCredit: Constants.USERS.CUSTODIAN,
		_userDebit: orderBuy._user,
		_token: pair._tokenBase,
		amount: trade.amountBase - commissionBuy,
	})

	transactionInsertUser2User({
		_userCredit: Constants.USERS.CUSTODIAN,
		_userDebit: Constants.USERS.BENEFICIARY,
		_token: pair._tokenQuoted,
		amount: commissionSell,
	})

	transactionInsertUser2User({
		_userCredit: Constants.USERS.CUSTODIAN,
		_userDebit: Constants.USERS.BENEFICIARY,
		_token: pair._tokenBase,
		amount: commissionBuy,
	})

	orderSell.amountBaseLeft -= trade.amountBase
	orderSell.flags.filled = orderSell.amountBaseLeft === 0

	orderBuy.amountBaseLeft -= trade.amountBase
	orderBuy.flags.filled = orderBuy.amountBaseLeft === 0

	Collections.Orders.update(orderSell._id, { $set: orderSell })
	Collections.Orders.update(orderBuy._id, { $set: orderBuy })

	Collections.Trades.insert(trade)

}