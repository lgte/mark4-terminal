import { Constants } from '/constants'
import { Collections } from '/collections'

export const candlesFind = (selector = {}, options = {}) => {
	return Collections.Candles.find(selector, { sort: { timestamp: 1 } })
}