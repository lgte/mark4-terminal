import { Synchronizations } from '/collections'

const defaults = {}

export const synchronizationsInsert = (data) => {
	check(data, {
		name: String,
		payload: Match.OneOf(Array, Object)
	})
	const log = Object.assign({
		createdAt: new Date()
	}, defaults, data)
	return Synchronizations.insert(log)
}

export const synchronizationsFind = (query = {}, options = {}) => Synchronizations.find(query, options)

export const synchronizationsFindRecent = (query = {}) => {
	return synchronizationsFind(query, {limit: 1, sort: {createdAt: -1}}).fetch()[0]
}