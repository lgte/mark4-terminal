import { Constants } from '/constants'
import { Collections } from '/collections'
import { notificationsInsert } from './notificationsInsert'

export const notificationsInsertBySuccess = res => {
	return notificationsInsert({
		_user: Meteor.userId(),
		title: 'Success',
		type: 'message'
	})
}