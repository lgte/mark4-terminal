import { Lib } from '/lib'
import { Constants } from '/constants'
import { Collections } from '/collections'

export const depositsFindOne = (selector, options) => {
	return Collections.Deposits.findOneOrDie(selector, options)
}