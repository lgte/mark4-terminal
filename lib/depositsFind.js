import { Lib } from '/lib'
import { Constants } from '/constants'
import { Collections } from '/collections'

export const depositsFind = (selector = {}, options = {}) => {
	Collections.Deposits.find(selector, options)
}