import _ from 'lodash'
import { Collections } from '/collections'
import { transactionsInsert } from './transactionsInsert'

export const transactionInsertUser2User = ({ _userCredit, _userDebit, _token, amount }) => {

	check(_userCredit, String)
	check(_userDebit, Match.OneOf(String, Array))
	check(_token, String)
	check(amount, Number)

	_.isArray(_userDebit) || (_userDebit = [_userDebit])

	let accountCredit = Collections.Accounts.findOneOrDie({ _user: _userCredit, _token })
	let accountsDebit = Collections.Accounts.findOrDie({ _user: { $in: _userDebit }, _token }, {}, _userDebit.length).fetch()

	//console.log(accountsDebit, { _user: { $in: _userDebit }, _token })

	accountsDebit.forEach(accountDebit  => {
		const transaction = {
			_accountCredit: accountCredit._id,
			_accountDebit: accountDebit._id,
			amount,
		}
		transactionsInsert(transaction)
		accountCredit = accountDebit
	})

}