import { Constants } from '/constants'
import { Collections } from '/collections'
import { wallets } from './wallets'

Collections.Wallets.find({ 'flags.used': false }).observe({
	added(wallet) {
		wallets[wallet._token] || (wallets[wallet._token] = [])
		wallets[wallet._token].push(wallet)
	},
})

export const walletsUse = (_user, _token) => {

	check(_user, String)
	check(_token, String)

	const wallet = wallets[_token].shift()

	if (!wallet) {
		throw new Error(`Can't generate wallet for ${_token}`)
	}

	Collections.Wallets.update(wallet._id, {
		$set: { 'flags.used': true, _user },
	})

	return Collections.Wallets.findOneOrDie(wallet._id)
}