import { Collections } from '/collections'
import { Accounts } from 'meteor/accounts-base'

export const usersFindByUsername = username => {
	check(username, String)
	const userByUsername = Accounts.findUserByUsername(username)
	const userById = Collections.Users.findOne(username)
	const user = userByUsername || userById
	if (!user) {
		throw new Error(`user ${username} not found`)
	}
	return user
}