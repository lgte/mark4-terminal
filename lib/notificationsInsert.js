import { Constants } from '/constants'
import { Collections } from '/collections'

export const notificationsInsert = notification => Collections.Notifications.insert(notification)