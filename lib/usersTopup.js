import { Constants } from '/constants'
import { Lib } from '/lib'

export const usersTopup = (_userAdapter, _user, _token, amount) => {
	check(_userAdapter, String)
	check(_user, String)
	check(_token, String)
	check(amount, Number)

	Lib.transactionInsertUser2User({
			_userCredit: Constants.USERS.ISSUER,
			_userDebit: [_userAdapter, Constants.USERS.AUDITOR_DEPOSIT, _user],
			_token: _token,
			amount: amount,
		},
	)

}