import { Collections } from '/collections'
import { Constants } from '../constants'

import { transactionInsertUser2User } from './transactionInsertUser2User'
import { accountsFindByUser } from './accountsFindByUser'
import { pairsFindOne } from './pairsFindOne'
import { tradesInsert } from './tradesInsert'
import { ordersFindActive } from './ordersFindActive'
import {matchingCalculateTrades} from './matchingCalculateTrades'

export const ordersInsert = (order) => {

	check(order._user, String)
	check(order._pair, String)
	check(order.amountBase, Number)
	check(order.type, String)
	check(order.direction, String)

	// Базовые сущности которые нам понадобятся в любом случае
	const pair = pairsFindOne(order._pair)
	order.amountBaseLeft = order.amountBase

	// если ордер на продажу значит мы хотим продать ETH за BTC
	// отдаем эфир получаем биток
	// amount - эфиры
	// amountQuoted - битки
	// коимссию берем в эфирахтак как это базовая валюта и она у нас есть

	if (order.type !== Constants.ORDER_TYPES.MARKET) {
		check(order.price, Number)
		order.amountQuoted = order.amountBase * order.price
		order.amountQuotedLeft = order.amountQuoted

		if (order.direction === Constants.ORDER_DIRECTIONS.SELL) {
			// если мы продаем эфир то нам надо захолдить его тоесть amountBase
			// холдим сразу вместе с комиссией так же будем возвращать при отмене

			order.commissionBase = order.amountBase * Constants.SETTINGS_GLOBAL.COMMISSION_MAKER

			transactionInsertUser2User({
				_userCredit: order._user,
				_userDebit: Constants.USERS.CUSTODIAN,
				_token: pair._tokenBase,
				amount: order.amountBase,
			})
		}

		if (order.direction === Constants.ORDER_DIRECTIONS.BUY) {
			// если мы покупаем эфир то нам нужно захолдить биток amountQuoted
			// раз у нас есть биток то и комиссию берем тоже в нем
			order.commissionQuoted = order.amountQuoted * Constants.SETTINGS_GLOBAL.COMMISSION_MAKER

			transactionInsertUser2User({
				_userCredit: order._user,
				_userDebit: Constants.USERS.CUSTODIAN,
				_token: pair._tokenQuoted,
				amount: order.amountQuoted,
			})
		}

		// Для мейкер ордера просто выходим он сидит в стакане и ждет тейкер
		return Collections.Orders.insert(order)

	}

	// то что нам понадобится только если мы будем матчикть маркет ордер
	const ordersActive = ordersFindActive({ _pair: order._pair }).fetch()
	const trades = matchingCalculateTrades(order, ordersActive)
	const amountBaseAvailable = trades.reduce((acc, cur) => acc += cur.amountBase, 0)

	if (amountBaseAvailable < order.amountBase) {
		throw new Error(`Not enough liquidity in OrderBook to handle ${order.amountBase} ${order._pair} ${order.direction}`)
	}

	order.amountQuoted = trades.reduce((acc, cur) => acc += cur.amountQuoted, 0)
	order.amountQuotedLeft = order.amountQuoted

	if (order.direction === Constants.ORDER_DIRECTIONS.SELL) {
		order.commissionBase = order.amountBase * Constants.SETTINGS_GLOBAL.COMMISSION_TAKER

		transactionInsertUser2User({
			_userCredit: order._user,
			_userDebit: Constants.USERS.CUSTODIAN,
			_token: pair._tokenBase,
			amount: order.amountBase,
		})

	}

	if (order.direction === Constants.ORDER_DIRECTIONS.BUY) {
		order.commissionQuoted = order.amountQuoted * Constants.SETTINGS_GLOBAL.COMMISSION_TAKER

		transactionInsertUser2User({
			_userCredit: order._user,
			_userDebit: Constants.USERS.CUSTODIAN,
			_token: pair._tokenQuoted,
			amount: order.amountQuoted,
		})

	}

	order._id = Collections.Orders.insert(order)

	trades.forEach(trade => {
		tradesInsert({
			...trade,
			[`_order${order.direction === Constants.ORDER_DIRECTIONS.SELL ? 'Sell' : 'Buy'}`]: order._id,
		})
	})

	return order._id
}