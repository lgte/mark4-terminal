import { Users } from '/collections'
import { Accounts } from 'meteor/accounts-base'

import { accountsInsertByUser } from './accountsInsertByUser'

export const usersInsert = data => {
	if (data._id) {
		const result = Users.upsert(data._id, { $set: data })
		result.insertedId && accountsInsertByUser(result.insertedId)
		return result.insertedId
	} else {
		const _user = Accounts.createUser(data)
		accountsInsertByUser(_user)
		return _user
	}
}