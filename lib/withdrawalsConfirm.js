import { Lib } from '/lib'
import { Constants } from '/constants'
import { Collections } from '/collections'

export const withdrawalsConfirm = (_withdrawal) => {
	const withdrawal = Collections.Withdrawals.findOneOrDie(_withdrawal)
	if (withdrawal.flags.confirmed || withdrawal.flags.cancelled) {
		throw new Error(`cannot confirm processed withdrawal ${_withdrawal}`)
	}
	Lib.transactionInsertUser2User({
		_userCredit: withdrawal._user,
		_userDebit: [Constants.USERS.AUDITOR_WITHDRAW, Constants.USERS.ISSUER],
		amount: withdrawal.amount,
		_token: withdrawal._token,
	})
	Collections.Withdrawals.update(_withdrawal, { $set: { 'flags.confirmed': true } })
}