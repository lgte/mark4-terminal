import { Collections } from '/collections'

export const tradesFind = (selector = {}, options = {}) => {
	return Collections.Trades.find(selector, options)
}