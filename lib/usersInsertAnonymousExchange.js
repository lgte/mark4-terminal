import { Meteor } from "meteor/meteor"
import { Tracker } from "meteor/tracker"
import { call, getObjectId } from '/stereor'
import { usersInsert } from './usersInsert'

export const usersInsertAnonymousExchange = () => new Promise((resolve, reject) => {
	Meteor.startup(() => {
		Tracker.autorun(function(computation) {
			if (Meteor.user() === null) {
				const token = getObjectId()
				const profile = {
					name: 'Anonymous',
					anonymous: true,
				}
				call('usersInsert', {
					username:  token,
					password: token,
					profile
				})
					.then(() => {
						computation.stop()
					})
					.catch(() => {
						computation.stop()
					})
			} else if (Meteor.user()) {
				computation.stop()
				resolve()
			}
		})
	})
})