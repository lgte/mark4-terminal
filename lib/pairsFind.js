import { Collections } from '/collections'

export const pairsFind = (query = {}, options = {}) => {
	return Collections.Pairs.find(query, options)
}
