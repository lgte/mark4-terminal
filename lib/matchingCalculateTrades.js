import { Constants } from '/constants'

export const matchingCalculateTrades = (order, ordersHeap) => {

	const comparator = order.direction === Constants.ORDER_DIRECTIONS.SELL
		? (a, b) => a.price > b.price
		: (a, b) => a.price < b.price
	const orders = ordersHeap.filter(o => o.direction != order.direction).sort(comparator)

	const result = orders.reduce((acc, cur) => {
		if (acc.amountBaseLeft === 0) {
			return acc
		}
		const amountBase = Math.min(acc.amountBaseLeft, cur.amountBaseLeft)
		const amountQuoted = amountBase * cur.price

		acc.amountBaseLeft -= amountBase
		acc.trades.push({
			[`_order${order.direction === Constants.ORDER_DIRECTIONS.SELL ? 'Sell' : 'Buy'}`]: order._id,
			[`_order${cur.direction === Constants.ORDER_DIRECTIONS.SELL ? 'Sell' : 'Buy'}`]: cur._id,
			amountBase,
			amountQuoted,
			price: cur.price,
			_pair: order._pair
		})
		return acc
	}, {
		amountBaseLeft: order.amountBaseLeft,
		trades: [],
	})

	return result.trades
}