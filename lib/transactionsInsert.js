import { Collections } from '/collections'

export const transactionsInsert = transaction => {

	check(transaction._accountCredit, String)
	check(transaction._accountDebit, String)
	check(transaction.amount, Number)

	const accounts = Collections.Accounts.findOrDie(
		{
			_id: { $in: [transaction._accountCredit, transaction._accountDebit] },
		},
		{},
		2
	).fetch()

	const accountCredit = accounts.find(el => el._id === transaction._accountCredit)
	const accountDebit = accounts.find(el => el._id === transaction._accountDebit)

	if (accountDebit._token !== accountCredit._token) {
		throw new Error(`${accountCredit._id} and ${accountDebit._id} token mismatch`)
	}

	if (accountCredit.balance < transaction.amount && accountCredit._user !== 'issuer') {
		throw new Error(
			`account ${accountCredit._id} has insufficient funds (creditBalance: ${accountCredit.balance}) for transferring ${transaction.amount}${accountCredit._token} to ${accountDebit._id}`,
		)
	}

	Collections.Transactions.insert(transaction)

	Collections.Accounts.update(transaction._accountCredit, { $inc: { balance: -transaction.amount } })
	Collections.Accounts.update(transaction._accountDebit, { $inc: { balance: transaction.amount } })

}