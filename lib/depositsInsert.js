import { Collections } from '/collections'
import { Constants } from '/constants'

export const depositsInsert = (deposit) => {
	check(deposit, Object)
	return Collections.Deposits.insert(deposit)
}