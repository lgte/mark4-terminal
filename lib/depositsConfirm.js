import { Lib } from '/lib'
import { Constants } from '/constants'
import { Collections } from '/collections'

export const depositsConfirm = (_deposit, amount) => {
	check(_deposit, String)
	check(amount, Number)
	const deposit = Collections.Deposits.findOneOrDie(_deposit)
	const _userAdapter = 'adapterIn' + deposit._token.charAt(0).toUpperCase() + deposit._token.slice(1)

	Lib.transactionInsertUser2User({
		_userCredit: Constants.USERS.ISSUER,
		_userDebit: [
			_userAdapter,
			Constants.USERS.AUDITOR_DEPOSIT,
			deposit._user,
		],
		_token: deposit._token,
		amount,
	})

	Collections.Deposits.update(_deposit, { $set: { 'flags.confirmed': true, amount } })
}