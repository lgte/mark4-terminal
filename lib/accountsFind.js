import { Collections } from '/collections'

export const accountsFind = (selector = {}, options = {}) => {
	return Collections.Accounts.find(selector, options)
}