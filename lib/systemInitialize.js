import * as initializationData from '/data/initialization'
import { Collections } from '/collections'

import { usersInsert } from './usersInsert'
import { tokensInsert } from './tokensInsert'
import { pairsInsert } from './pairsInsert'

export const systemInitialize = () => {

	initializationData.Users.map(usersInsert)
	initializationData.Tokens.map(tokensInsert)
	initializationData.Pairs.map(pairsInsert)

	const systemStat = {
		restartTime: new Date,
		usersCount: Collections.Users.find().count(),
		tokensCount: Collections.Tokens.find().count(),
		accountsCount: Collections.Accounts.find().count(),
		pairsCount: Collections.Tokens.find().count(),
	}

	const systemCheck = {
		usersXTokensEqAccounts: systemStat.usersCount * systemStat.tokensCount === systemStat.accountsCount,
	}

	console.log('===================== MARK4 =====================')
	console.table(systemStat)
	console.table(systemCheck)
	console.log('===================== MARK4 =====================')

}