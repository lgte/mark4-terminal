import { Users } from '/collections'
import { Tokens } from '/collections'
import { Accounts } from '/collections'

export const tokensInsert = data => {
	const result = Tokens.upsert(data._id, { $set: data })
	const users = Users.find().fetch()

	result.insertedId && users.forEach(user => {
		Accounts.upsert(
			{ _user: user._id, _token: result.insertedId },
			{
				$set: { _user: user._id, _token: result.insertedId },
				$setOnInsert: { balance: 0 },
			},
		)
	})

}