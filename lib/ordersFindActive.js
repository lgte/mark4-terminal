import { Collections } from '/collections'
import { Constants } from '/constants'

export const ordersFindActive = (selector = {}) => {
	check(selector, Object)
	const query = {
		...Constants.ORDER_QUERY_ACTIVE,
		...selector,
		type: {$ne: Constants.ORDER_TYPES.MARKET}
	}
	return Collections.Orders.find(query)
}