import { systemReset as stereorSystemReset } from '/stereor/server/systemReset'
import { wallets } from './wallets'

export const systemReset = () => {
	Object.keys(wallets).forEach(key => {
		delete wallets[key]
	})
	stereorSystemReset()
	console.log('system reset to initial state')
}