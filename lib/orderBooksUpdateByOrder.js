import * as collections from '/collections'
import * as constants from '/constants'

const timers = {}

export const orderBooksUpdateByOrder = doc => {
	Meteor.clearTimeout(timers[doc._pair])

	timers[doc._pair] = Meteor.setTimeout(() => {
		const orderBook = { _pair: doc._pair, sell: [], buy: [] }
		const queryBuy = {
			...constants.ORDER_QUERY_ACTIVE,
			_pair: doc._pair,
			direction: 'buy',
			type: { $ne: constants.ORDER_TYPES.MARKET },
		}
		const querySell = {
			...constants.ORDER_QUERY_ACTIVE,
			_pair: doc._pair,
			direction: 'sell',
			type: { $ne: constants.ORDER_TYPES.MARKET },
		}

		orderBook.sell = collections.Orders.find(querySell, { sort: { price: 1 }, limit: 20 }).fetch()
		orderBook.buy = collections.Orders.find(queryBuy, { sort: { price: -1 }, limit: 20 }).fetch()

		collections.OrderBooks.upsert({ _pair: doc._pair }, { $set: orderBook })
	}, 1000)

}