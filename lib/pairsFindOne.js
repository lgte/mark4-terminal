import { Collections } from '/collections'

export const pairsFindOne = (selector = {}) => {
	return Collections.Pairs.findOneOrDie(selector)
}