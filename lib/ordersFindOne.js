import { Collections } from '/collections'

export const ordersFindOne = (selector = {}) => {
	return Collections.Orders.findOneOrDie(selector)
}