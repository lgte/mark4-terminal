import { Collections } from '/collections'
import { Constants } from '/constants'

export const ordersFind = (selector = {}) => {
	return Collections.Orders.find(selector)
}