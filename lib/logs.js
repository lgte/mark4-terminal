import { Logs } from '/collections'
import { Constants } from '/constants'

const defaults = {
	group: 'main',
	level: 'message', //error, warning
}

export const logsInsert = (data, toConsole = false) => {
	check(data, Object)
	check(data.group, String)
	check(data.level, String)
	check(data.message, String)

	const log = Object.assign({
		createdAt: new Date()
	}, defaults, data)
	toConsole && console.log(log)

	return Logs.insert(log)
}

