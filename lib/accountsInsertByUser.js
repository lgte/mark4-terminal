import { Tokens } from '/collections'
import { Accounts } from '/collections'

export const accountsInsertByUser = _user => {
	check(_user, String)
	const tokens = Tokens.find().fetch()
	tokens.forEach(token => {
		Accounts.upsert(
			{ _user, _token: token._id },
			{
				$set: { _user, _token: token._id },
				$setOnInsert: { balance: 0 },
			},
		)
	})

}