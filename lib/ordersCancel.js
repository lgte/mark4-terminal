import { Collections } from '/collections'
import { Constants } from '/constants'

import { tradesFind } from './tradesFind'
import { transactionInsertUser2User } from './transactionInsertUser2User'

export const ordersCancel = (_order) => {
	check(_order, String)
	const order = Collections.Orders.findOneOrDie(_order)
	const pair = Collections.Pairs.findOneOrDie(order._pair)
	const trades = tradesFind({ _order: order._id }).fetch()
	const amountBase = trades.reduce((a, c) => a += c.amountBase, 0)
	const amountQuoted = trades.reduce((a, c) => a += c.amountQuoted, 0)

	if (order.direction === Constants.ORDER_DIRECTIONS.SELL) {
		transactionInsertUser2User({
			_userCredit: Constants.USERS.CUSTODIAN,
			_userDebit: order._user,
			_token: pair._tokenBase,
			amount: order.amountBase - amountBase
		})
	} else {
		transactionInsertUser2User({
			_userCredit: Constants.USERS.CUSTODIAN,
			_userDebit: order._user,
			_token: pair._tokenQuoted,
			amount: order.amountQuoted - amountQuoted
		})
	}
	Collections.Orders.update(order._id, {$set: {'flags.cancelled': true}})

	return order
}