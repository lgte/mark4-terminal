export const systemStartInterval = (func, runOnStart = true, timeout = 10000) => {
	runOnStart && func()
	Meteor.setInterval(func, timeout)
}