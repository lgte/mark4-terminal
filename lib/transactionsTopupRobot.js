import { Accounts } from 'meteor/accounts-base'
import { tokensFind } from './tokensFind'
import { transactionInsertUser2User } from './transactionInsertUser2User'

import { Constants } from '/constants'

export const transactionsTopupRobot = (username, _token, amount = 100) => {
	check(username, String)
	check(_token, String)
	check(amount, Number)

	const robot = Accounts.findUserByUsername(username)
	if (!robot) {
		throw new Error(`robot with ${username} not found`)
	}
	const tokens = tokensFind({_id: _token}).fetch()

	tokens.forEach(token => {
		transactionInsertUser2User({
			_userCredit: Constants.USERS.ISSUER,
			_userDebit: [Constants.USERS.AUDITOR_ROBOTS, robot._id],
			_token: token._id,
			amount,
		})
	})

}