import { Collections } from '/collections'

export const transactionsFindByAccount = (_account) => {
	check(_account, String)
	return Collections.Transactions.find({_account})
}