import { Collections } from '/collections'

export const candlesUpsertByTrade = (trade, timeFrame = 'minute') => {

	const time = moment(trade.createdAt).startOf(timeFrame).toDate()
	const timestamp = moment(trade.createdAt).startOf(timeFrame).format('x')

	Collections.Candles.upsert({ time, _pair: trade._pair, timeFrame }, {
		$setOnInsert: {
			time,
			timestamp: timestamp,
			timeFrame: timeFrame,
			_pair: trade._pair,
			open: trade.price,
			close: trade.price,
			high: trade.price,
			low: trade.price,
		},
		$inc: {
			volume: trade.amountBase,
		},
	})

	Collections.Candles.update(
		{ time, _pair: trade._pair, timeFrame },
		{ $set: { close: trade.price } },
	)

	Collections.Candles.update(
		{ time, _pair: trade._pair, timeFrame, high: { $lt: trade.price } },
		{ $set: { high: trade.price } },
	)

	Collections.Candles.update(
		{ time, _pair: trade._pair, timeFrame, low: { $gt: trade.price } },
		{ $set: { low: trade.price } },
	)

}