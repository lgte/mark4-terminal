import { Tools } from './tools'
import { Lib } from '/lib'
import { Constants } from '/constants'

export const testUserTopupWithWallet = async () => {

	Tools.createUsers(1)

	const _token = 'btc'
	const amount = 10

	const wallet = Lib.walletsUse(Lib.usersFindByUsername('test-user-0')._id, _token)
	const _deposit = Lib.depositsInsert({
		_wallet: wallet._id,
		_user: Lib.usersFindByUsername('test-user-0')._id,
		_token
	})

	Lib.depositsConfirm(_deposit, amount)

	const auditorAccount = Lib.accountFindOneByUser(Lib.usersFindByUsername(Constants.USERS.AUDITOR_DEPOSIT)._id, _token)

	return {
		userBalance: Lib.accountFindOneByUser(Lib.usersFindByUsername('test-user-0')._id, _token).balance,
		auditorBalance: auditorAccount.balance,
		auditorTurnover: Lib.transactionsFindByAccount(auditorAccount._id).fetch().reduce((a, c) => a += c.amount, 0)
	}
}
