import { Tasks } from '/tasks'
import { Constants } from '/constants'
import { Lib } from '/lib'
import { Accounts } from 'meteor/accounts-base'
import { Queue } from '/stereor/Queue'

const username1 = 'test-1'
const username2 = 'test-2'
const password = 'test-1'
const _pair = 'btcusd'

export const ordersInsertMaker = () => {

	const direction = Constants.ORDER_DIRECTIONS.BUY

	Tasks.usersInsert({ username: username1, password })
	Tasks.usersInsert({ username: username2, password })

	Queue.push(() => {

		const user1 = Accounts.findUserByUsername(username1)
		const user2 = Accounts.findUserByUsername(username2)
		const pair = Lib.pairsFindOne(_pair)

		Lib.transactionInsertUser2User({
			_userCredit: Constants.USERS.ISSUER,
			_userDebit: user1._id,
			_token: pair._tokenBase,
			amount: 100000,
		})

		Lib.transactionInsertUser2User({
			_userCredit: Constants.USERS.ISSUER,
			_userDebit: user1._id,
			_token: pair._tokenQuoted,
			amount: 100000,
		})

		Lib.transactionInsertUser2User({
			_userCredit: Constants.USERS.ISSUER,
			_userDebit: user2._id,
			_token: pair._tokenBase,
			amount: 100000,
		})

		Lib.transactionInsertUser2User({
			_userCredit: Constants.USERS.ISSUER,
			_userDebit: user2._id,
			_token: pair._tokenQuoted,
			amount: 100000,
		})

		Tasks.ordersInsert({
			direction: direction,
			type: Constants.ORDER_TYPES.LIMIT,
			amountBase: 1,
			price: 13000,
			_pair: _pair,
			_user: user1._id,
		})

		Tasks.ordersInsert({
			direction: direction,
			type: Constants.ORDER_TYPES.LIMIT,
			amountBase: 1,
			price: 13500,
			_pair: _pair,
			_user: user1._id,
		})

		Tasks.ordersInsert({
			direction: direction === Constants.ORDER_DIRECTIONS.SELL
				? Constants.ORDER_DIRECTIONS.BUY
				: Constants.ORDER_DIRECTIONS.SELL,
			type: Constants.ORDER_TYPES.MARKET,
			amountBase: 1,
			_pair: _pair,
			_user: user2._id,
		})

	}, 'preparing test')

	//return
	Queue.push(() => {
		Meteor.setTimeout(() => {
			const user1 = Accounts.findUserByUsername(username1)
			const user2 = Accounts.findUserByUsername(username2)
			const pair = Lib.pairsFindOne(_pair)

			console.log('----------')

			console.log(`balance issuer ${pair._tokenBase}`, Lib.accountFindOneByUser(Constants.USERS.ISSUER, pair._tokenBase).balance)
			console.log(`balance issuer ${pair._tokenQuoted}`, Lib.accountFindOneByUser(Constants.USERS.ISSUER, pair._tokenQuoted).balance)

			console.log(`balance custodian ${pair._tokenBase}`, Lib.accountFindOneByUser(Constants.USERS.CUSTODIAN, pair._tokenBase).balance)
			console.log(`balance custodian ${pair._tokenQuoted}`, Lib.accountFindOneByUser(Constants.USERS.CUSTODIAN, pair._tokenQuoted).balance)

			console.log(`balance beneficiary ${pair._tokenBase}`, Lib.accountFindOneByUser(Constants.USERS.BENEFICIARY, pair._tokenBase).balance)
			console.log(`balance beneficiary ${pair._tokenQuoted}`, Lib.accountFindOneByUser(Constants.USERS.BENEFICIARY, pair._tokenQuoted).balance)

			console.log(`balance user1: ${user1._id} ${pair._tokenBase}`, Lib.accountFindOneByUser(user1._id, pair._tokenBase).balance)
			console.log(`balance user1: ${user1._id} ${pair._tokenQuoted}`, Lib.accountFindOneByUser(user1._id, pair._tokenQuoted).balance)

			console.log(`balance user2: ${user2._id} ${pair._tokenBase}`, Lib.accountFindOneByUser(user2._id, pair._tokenBase).balance)
			console.log(`balance user2: ${user2._id} ${pair._tokenQuoted}`, Lib.accountFindOneByUser(user2._id, pair._tokenQuoted).balance)

		}, 2000)
	}, 'startingTimeout')

}