import { Tools } from './tools'
import { Lib } from '/lib'
import { Constants } from '/constants'

export const testOrdersCancelQuotedUnfilled = async () => {
	Tools.createUsers(1)
	const user = Lib.usersFindByUsername('test-user-0')
	const _token = 'usdt'
	const _pair = 'btcusd'
	const amount = 100000
	Lib.usersTopup('adapterInManual', user._id, _token, amount)

	const balanceBefore = Lib.accountFindOneByUser(user._id, _token).balance

	const _order = Lib.ordersInsert({
		direction: Constants.ORDER_DIRECTIONS.BUY,
		type: Constants.ORDER_TYPES.LIMIT,
		amountBase: 5,
		price: 13000,
		_user: user._id,
		_pair
	})

	const ordersActive = Lib.ordersFindActive().fetch()
	const balanceAfterOrder = Lib.accountFindOneByUser(user._id, _token).balance
	const balanceCustodian = Lib.accountFindOneByUser(Constants.USERS.CUSTODIAN, _token).balance

	Lib.ordersCancel(_order)

	const balanceAfterOrderCancel = Lib.accountFindOneByUser(user._id, _token).balance
	const balanceCustodianAfterCancel = Lib.accountFindOneByUser(Constants.USERS.CUSTODIAN, _token).balance
	return {
		balanceBefore,
		balanceAfterOrder,
		balanceCustodian,
		balanceAfterOrderCancel,
		balanceCustodianAfterCancel,
		ordersActiveCount: ordersActive.length,
		ordersActiveCountAfterCancel: Lib.ordersFindActive().fetch().length
	}
}
