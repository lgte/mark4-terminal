import { Tools } from './tools'
import { Lib } from '/lib'
import { Constants } from '/constants'

export const testUsersTopup = async () => {
	// Создать юзера
	// Пополнить его через аудитора manual на 10 btc
	// Провероить баланс issuer = -10btc
	// Проверить баланс manual = 0
	// Проверить приход на manual = 10
	// Проверить на manual = -10
	Tools.createUsers(1)
	const _user = Lib.usersFindByUsername('test-user-0')._id
	const _token = 'btc'
	const _userAdapter = 'adapterInManual'
	const amount = 10
	Lib.usersTopup(
		_userAdapter,
		_user,
		_token,
		amount
	)
	const output = {
		issuerBalance: Lib.accountFindOneByUser(Constants.USERS.ISSUER, _token).balance,
		adapterManualBalance: Lib.accountFindOneByUser(_userAdapter, _token).balance,
		adapterManualCashFlow: Lib.transactionsFindByAccount(Lib.accountFindOneByUser(_userAdapter, _token)._id).fetch().reduce((a, c) => a += c.amount, 0),
		userBalance: Lib.accountFindOneByUser(_user, _token).balance,
	}
	return output
}
