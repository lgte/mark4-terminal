import { Queue } from '/stereor/Queue'
import _ from 'lodash'

export const testQueue = () => {
	const doRandomDelay = () => new Promise(resolve => {
		const delay = _.random(2000, 3000)
		setTimeout(() => {
			console.log(`delayed for ${delay}ms`)
			resolve()
		}, delay)
	})

	Queue.push(doRandomDelay, 'randomDelay')
	Queue.push(() => {
		console.log('sayHi1')
	}, 'sayHi')
	Queue.push(doRandomDelay, 'randomDelay')
	Queue.push(() => {
		console.log('sayHi2')
	}, 'sayHi')
	Queue.push(doRandomDelay, 'randomDelay')
	Queue.push(() => {
		console.log('sayHi3')
	}, 'sayHi')

	console.log('----')

	Queue.run()

}


