import { Tools } from './tools'
import { Lib } from '/lib'
import { Constants } from '../constants'

export const testUserWithdrawEnoughFunds = async () => {
	Tools.createUsers(1)
	const _user = Lib.usersFindByUsername('test-user-0')._id
	const _token = 'btc'
	const _userAdapter = 'adapterInManual'
	const amount = 10

	Lib.usersTopup(
		_userAdapter,
		_user,
		_token,
		amount
	)

	const _withdrawal = Lib.withdrawalsInsert({
		_user,
		_token,
		amount: amount / 2,
		address: {
			address: 'asjdhgfg1oe8r1tiuegdkahsjgdka'
		}
	})

	Lib.withdrawalsConfirm(_withdrawal)

	return {
		userBalance: Lib.accountFindOneByUser(_user, _token).balance,
		accountAuditorWithdrawTurnover: Lib.transactionsFindByAccount(Lib.accountFindOneByUser(Constants.USERS.AUDITOR_WITHDRAW)._id, _token)
			.fetch()
			.reduce((c, a) => c += a.amount, 0)
	}
}
