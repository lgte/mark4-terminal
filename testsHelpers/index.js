import * as TestsHelpers from './@.js'
import { Queue } from '/stereor/Queue'
import { Lib } from '../lib'

export { TestsHelpers }

export const Tester = {
	async run(tests) {
		const keys = tests || Object.keys(TestsHelpers)
		console.log('[- - - M4 Self Test - - -]')
		keys.forEach(key => {

			Queue.push(async () => {
				await Lib.systemReset()
				await Lib.systemInitialize()
			}, 'systemReset before ' + key)

			Queue.push(async () => {
				await TestsHelpers[key]()
					.then(r => {
						console.log(key, 'success')
						console.table(r)
					})
					.catch(console.warn)

			}, 'testing ' + key)
		})
		Queue.run(true)
	},
}