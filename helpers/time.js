
export const time = dt => {
	return moment.utc(dt).format('hh:mm:ss')
}