
export const dateTime = dt => {
	return moment(dt).format('DD/MM/YYYY HH:mm:ss')
}

export const formatDateTime = dateTime
