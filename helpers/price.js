import _ from 'lodash'

export const price = (price, precision = 8) => {
	return _.round(price, precision)
}

export const formatPrice = price