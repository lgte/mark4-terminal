import { Collection } from '/stereor'
import * as schemas from '/schemas'
import * as constants from '/constants'

export const Orders = new Collection(
	'orders',
	{
		_pair: String,
		_user: String,
		createdAt: schemas.datedAt(),
		direction: {
			type: String,
			allowedValues: Object.values(constants.ORDER_DIRECTIONS),
		},
		type: {
			type: String,
			allowedValues: Object.values(constants.ORDER_TYPES),
		},
		price: {
			type: Number,
			defaultValue: 0,
			optional: true,
		},
		amountBase: Number,
		commissionBase: {
			type: Number,
			defaultValue: 0,
		},
		amountBaseLeft: {
			type: Number,
			optional: true,
			autoValue() {
				if (!this.isSet) {
					return this.field('amountBase').value
				}
			},
		},
		amountQuoted: Number,
		commissionQuoted: {
			type: Number,
			defaultValue: 0,
		},
		amountLeftQuoted: {
			type: Number,
			optional: true,
			autoValue() {
				if (!this.isSet) {
					return this.field('amountQuoted').value
				}
			},
		},
		// разницав  Id и Tag в том что Id должен быть уникальным в рамках одного юзареа
		clientId: {
			type: String,
			optional: true
		},
		clientTag: {
			type: String,
			optional: true
		},
		flags: {
			type: Object,
			optional: true,
			defaultValue: {
				filled: false,
				cancelled: false,
			},
		},
		'flags.filled': Boolean,
		'flags.cancelled': Boolean,
	},
)