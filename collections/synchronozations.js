import { Collection } from '/stereor'
import * as schemas from '/schemas'

export const Synchronizations = new Collection(
	'synchronizations',
	false
)
