import { Collection } from '/stereor'
import * as schemas from '/schemas'

export const Withdrawals = new Collection(
	'withdrawals',
	{
		_user: String,
		wallet: {
			type: Object,
		},
		'wallet.address': String,
		'wallet.destinationTag': {
			type: String,
			optional: true
		},
		amount: {
			type: Number,
			optional: true,
			defaultValue: 0,
		},
		_token: String,
		flags: {
			type: Object,
			defaultValue: {
				'confirmed': false,
				'rejected': false
			}
		},
		'flags.confirmed': Boolean,
		'flags.rejected': Boolean,
	}
)
