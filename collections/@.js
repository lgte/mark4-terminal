import { Collection } from '/stereor'
import * as schemas from '/schemas'
import * as constants from '/constants'

export * from './synchronozations'
export * from './logs'
export * from './orders'
export * from './withdrawals'
export * from './deposits'
export * from './notifications'
export * from './prices'

export const Users = Meteor.users

export const Wallets = new Collection(
	'wallets',
	{
		_token: String,
		_user: {
			type: String,
			optional: true,
		},
		address: String,
		destinationTag: {
			type: String,
			optional: true,
		},
		flags: {
			type: Object,
			defaultValue: {
				used: false,
			},
		},
		'flags.used': Boolean,
	},
)

export const Params = new Collection(
	'params',
	{
		_id: String,
		value: Match.OneOf(String, Number, Date),
	},
)

export const Candles = new Collection(
	'candles',
	{
		timeFrame: String,
		time: Date,
		timestamp: Number,
		_pair: String,
		open: {
			type: Number,
			optional: true,
		},
		high: {
			type: Number,
			optional: true,
		},
		close: {
			type: Number,
			optional: true,
		},
		low: {
			type: Number,
			optional: true,
		},
		volume: {
			type: Number,
			defaultValue: 0,
			optional: true,
		},
	},
)

export const PaymentsAdapters = new Collection(
	'paymentsAdapters',
	{
		_id: String,
		_userAccountant: String,
		_token: String,
		title: String,
		explorerUrl: String,
		credentialsFields: Array,
		'credentialsFields.$': String,
	},
)

export const Withdraws = new Collection(
	'withdraws',
	{
		_user: schemas.id(),
		_token: schemas.id(),
		createdAt: schemas.datedAt(),
		address: String,
		destinationTag: {
			type: String,
			optional: true,
		},
		amount: Number,
		flags: {
			type: Object,
			defaultValue: {
				confirmed: false,
				rejected: false,
			},
		},
		'flags.confirmed': Boolean,
		'flags.rejected': Boolean,
	},
)

export const Payments = new Collection(
	'payments',
	{
		_token: {
			type: String,
		},
		_wallet: {
			type: String,
		},
		_user: {
			type: String,
		},
		amount: {
			type: Number,
			defaultValue: 0,
		},
		transactionHash: {
			type: String,
			optional: true,
		},
		direction: {
			type: String,
			allowedValues: ['in', 'out'],
			defaultValue: 'in',
		},
		flags: {
			type: Object,
			defaultValue: {
				confirmed: false,
			},
		},
		'flags.confirmed': Boolean,
	},
)

export const Tokens = new Collection(
	'tokens',
	{
		_id: String,
		createdAt: schemas.datedAt(),
		title: String,
		symbol: String,
	})

export const Pairs = new Collection(
	'pairs',
	{
		_id: String,
		_tokenBase: String,
		_tokenQuoted: String,
		price: {
			type: Number,
			defaultValue: 0,
		},
		change: {
			type: Number,
			defaultValue: 0,
		},
	},
	{
		inMemory: true,
	},
)

export const Trades = new Collection(
	'trades',
	{
		createdAt: schemas.datedAt(),
		_orderSell: String,
		_orderBuy: String,
		_pair: String,
		amountBase: Number,
		amountQuoted: Number,
		price: Number,
		_order: {
			type: Array,
			autoValue() {
				return [
					this.field('_orderSell').value,
					this.field('_orderBuy').value,
				]
			},
		},
		'_order.$': String,
	},
)

export const OrderBooks = new Collection(
	'orderBooks',
	{
		_pair: String,
		sell: Array,
		'sell.$': {
			type: Object,
			blackbox: true,
		},
		buy: Array,
		'buy.$': {
			type: Object,
			blackbox: true,
		},
	},
)

export const Accounts = new Collection(
	'accounts',
	{
		_user: String,
		_token: String,
		balance: {
			type: Number,
			defaultValue: 0,
		},
	},
	{
		inMemory: true
	},
)

export const Transactions = new Collection(
	'transactions',
	{
		createdAt: schemas.datedAt(),
		_accountCredit: String,
		_accountDebit: String,
		_account: {
			type: Array,
			autoValue() {
				return [
					this.field('_accountCredit').value,
					this.field('_accountDebit').value,
				]
			},
		},
		'_account.$': String,
		amount: Number,
		_user: {
			type: Array,
			autoValue() {
				const accountCredit = Accounts.findOneOrDie(this.field('_accountCredit').value)
				const accountDebit = Accounts.findOneOrDie(this.field('_accountDebit').value)
				return [accountCredit._user, accountDebit._user]
			},
		},
		'_user.$': {
			type: String,
		},
		comment: {
			type: String,
			optional: true,
		},
		payload: {
			type: Object,
			optional: true,
		},
	})

