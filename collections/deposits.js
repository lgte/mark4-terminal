import { Collection } from '/stereor'
import * as schemas from '/schemas'

export const Deposits = new Collection(
	'deposits',
	{
		_user: String,
		_wallet: String,
		_token: String,
		amount: {
			type: Number,
			optional: true,
			defaultValue: 0,
		},
		flags: {
			type: Object,
			defaultValue: {
				'confirmed': false,
				'rejected': false
			}
		},
		'flags.confirmed': Boolean,
		'flags.rejected': Boolean,
	}
)
