import { Collection } from '/stereor'
import * as schemas from '/schemas'

export const Logs = new Collection(
	'logs',
	false,
)
