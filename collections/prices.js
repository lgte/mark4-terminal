import { Collection } from '/stereor'
import * as schemas from '/schemas'

export const Prices = new Collection(
	'prices',
	{
		_pair: String,
		createdAt: schemas.datedAt(),
		price: Number
	}
)
