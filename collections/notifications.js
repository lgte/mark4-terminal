import { Collection } from '/stereor'
import * as schemas from '/schemas'

export const Notifications = new Collection(
	'notifications',
	{
		_user: String,
		createdAt: schemas.datedAt(),
		title: String,
		type: schemas.string('message'),
		payload: {
			type: Object,
			optional: true,
		},
		description: schemas.string(),
		...schemas.flags({
			shown: false,
		}),
	},
)
