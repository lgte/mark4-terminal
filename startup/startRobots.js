import { Mark1 } from '/robots/mark1'
import { Mark2 } from '/robots/mark2'

import { Constants } from '/constants'
import { Lib } from '/lib'

export const startRobots = () => {
	//const pairsSelector = { _id: 'ethusd' }
	const pairsSelector = { }
	const robotsServer = Meteor.settings.robotsServer

	Lib.pairsFind(pairsSelector).fetch().map(p => {
		console.log('starting mark 2', p._id)

		const _pair = p._id
		const usernameMaker = 'mark1-maker-' + _pair
		const usernameTaker = 'mark1-maker-' + _pair
		const password = 'robots0987654321!@#$$#@!'

		new Mark1({
			_pair,
			server: robotsServer,
			username: usernameMaker,
			password: password,
		})

		new Mark2({
			_pair,
			server: robotsServer,
			username: usernameTaker,
			password: password,
		})

	})

}