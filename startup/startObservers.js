import * as collections from '/collections'
import { Lib } from '/lib'
import { Constants } from '/constants'

/**
 * Обзерверы должны быть асинхронными в обзерверхах никогда нельзя осчитывать критичну информацию
 * например обновлеять балансы и так далее в обзерверах можно делать только вычисления целостность которых
 * не надо гарантировать, например статситику
 * @returns {Promise<void>}
 */
export const startObservers = async () => {
	let initialized = false

	collections.Orders.find({
		...Constants.ORDER_QUERY_ACTIVE
	}, { sort: { createdAt: -1 }, limit: 1000 }).observe({
		added: (doc) => initialized && Lib.orderBooksUpdateByOrder(doc),
		changed: (doc) => initialized && Lib.orderBooksUpdateByOrder(doc),
	})

	collections.Trades.find({}, { sort: { createdAt: -1 }, limit: 1000 }).observe({
		async added(trade) {
			if (!initialized) {
				return
			}

			const pair = collections.Pairs.findOneOrDie(trade._pair)

			collections.Pairs.update(trade._pair, {
				$set: {
					price: trade.price,
					change: trade.price - pair.price,
				},
			})

			Lib.candlesUpsertByTrade(trade, 'minute')
			Lib.candlesUpsertByTrade(trade, 'day')
		},
	})

	initialized = true
}