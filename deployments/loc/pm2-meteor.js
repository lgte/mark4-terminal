const appName = 'mark4-loc'
const mongoUrl = 'mongodb://root:abc123navcbsmdfwhsdgf134!@95.217.45.56:27017/mark4-loc?authSource=admin'
const host = '95.217.45.56'
const port = 2800

module.exports = {
	appName,
	'appLocation': {
		'local': `../..`,
	},
	'meteorSettingsLocation': `../../settings.loc.json`,
	'prebuildScript': '',
	'meteorBuildFlags': '--architecture os.linux.x86_64',
	'env': {
		'PORT': port,
		'MONGO_URL': mongoUrl,
		'ROOT_URL': `http://${host}:${port}`,
		'TZ': 'Europe/GMT',
	},
	'server': {
		'host': host,
		'username': 'root',
		'pem': '~/.ssh/id_rsa',
		'deploymentDir': '/home/rusinozemtsev/meteor-apps',
		'exec_mode': 'cluster_mode',
		'instances': 1,
	},
}