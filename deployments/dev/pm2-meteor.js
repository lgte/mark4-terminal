const appName = 'mark4-dev'
const mongoUrl = 'mongodb://root:qwertyui123456789lkjhgtyujnbvcds@127.0.0.1:27017/mark4-dev-3?authSource=admin'
const host = '95.217.45.56'
const port = 2800

module.exports = {
	appName,
	'appLocation': {
		'local': `../..`,
	},
	'meteorSettingsLocation': `./settings.json`,
	'prebuildScript': '',
	'meteorBuildFlags': '--architecture os.linux.x86_64',
	'env': {
		'PORT': port,
		'MONGO_URL': mongoUrl,
		'ROOT_URL': `https://exchange.lgte.io`,
		'TZ': 'Europe/GMT',
	},
	'server': {
		'host': host,
		'username': 'mark4',
		'password': 'mark452759401!',
		'deploymentDir': '/home/mark4/meteor-apps',
		'exec_mode': 'cluster_mode',
		'instances': 1,
	},
}