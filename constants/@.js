export const ORDER_TYPES = {
	MARKET: 'market',
	LIMIT: 'limit',
	STOP: 'stop',
}

export const ORDER_DIRECTIONS = {
	SELL: 'sell',
	BUY: 'buy'
}

export const ORDER_QUERY_ACTIVE = {
	'flags.cancelled': { $ne: true },
	'flags.filled': { $ne: true },
}

export const NOTIFICATION_TYPE = {
	ERROR: 'error',
	MESSAGE: 'message',
	CONFIRMATION: 'confirmation'
}

export const LOG_GROUP = {
	SYSTEM: 'system',
	MATCHING: 'matching',
	TRANSACTION: 'transactions',
	USER: 'user'
}

export const USERS = {
	CUSTODIAN: 'custodian',
	BENEFICIARY: 'beneficiary',
	ISSUER: 'issuer',
	AUDITOR_ROBOTS: 'auditorRobots',
	AUDITOR_DEPOSIT: 'auditorDeposit',
	AUDITOR_WITHDRAW: 'auditorWithdraw'
}

export const SETTINGS_GLOBAL = {
	COMMISSION_MAKER: 0.001,
	COMMISSION_TAKER: 0.002,
}
