import React from 'react'
import { mapItems, getWithTracker, call, clientUser } from '/stereor'
import { WaitForData } from '../WaitForData'

export const AuthBadgeForm = (props) => {
	const { children, user } = props
	return (
		<div className="content">
			<WaitForData waitFor={[user]}>
				logged as
				<br/>
				_id: {user && user._id}<br/>
				token: {user && user.username}

			</WaitForData>
		</div>
	)
}

export const AuthBadgeFormTracker = getWithTracker(props => {
	return {
		user: clientUser()
	}
}, props => {
	return []
})(AuthBadgeForm)




