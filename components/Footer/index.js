import React from 'react'
import PropTypes from 'prop-types'

export const Footer = ({}) => {
	return (
		<footer className="app-footer">
			<div className="content">
				<ul className="footer">
					<li>
						<div className="footer-title">2018 — 2019 TOKEN EXCHANGE</div>
						<ul className="list-vertical">
							<li>Secure and reliable crypto currencies exchange</li>
							<li><a href="#">Jobs</a></li>
						</ul>
					</li>
					<li>
						<div className="footer-title">Legal</div>
						<ul className="list-vertical">
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms Of Use</a></li>
						</ul>
					</li>
					<li>
						<div className="footer-title">For Users</div>
						<ul className="list-vertical">
							<li><a href="#">Support</a></li>
							<li><a href="#">Listing</a></li>
						</ul>
					</li>
					<li>
						<div className="footer-title">Other</div>
						<ul className="list-vertical">
							<li>Server Time 13:30</li>
							<li><a href="#">Twitter</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</footer>
	)
}

Footer.propTypes = {}

Footer.defaultProps = {}

