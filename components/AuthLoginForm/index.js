import React, { useState } from 'react'
import { mapItems, getWithTracker, getForm, call, callbackHandler } from '/stereor'
import { Accounts } from 'meteor/accounts-base'

export const AuthLoginForm = (props) => {
	const { children } = props
	const form = getForm({})
	return (
		<>
			<form {...form.getFormAttributes(e => {
				Meteor.loginWithPassword(
					e.target.value.email,
					e.target.value.password,
					callbackHandler
				)
			})}>

				<label>Email</label>
				<input type="text" {...form.getInputAttributes('email')} />

				<label>Password</label>
				<input type="password" {...form.getInputAttributes('password')} />

				<button type="submit">Login</button>

			</form>
		</>
	)
}

