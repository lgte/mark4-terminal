import React from 'react'
import { getWithTracker, mapItems } from '/stereor'
import { time } from '/helpers/time'
import * as collections from '/collections'
import { price } from '/helpers/price'

const Tr = props => {
	const { item } = props
	return (
		<tr>
			<td>{price(item.amountBase)}</td>
			<td>{price(item.price)}</td>
			<td>{time(item.createdAt)}</td>
		</tr>
	)
}

export const TradesHistory = (props) => {
	const { trades } = props
	return (
		<table className={'tradesHistory'}>
			<thead>
			<tr>
				<th>Amount</th>
				<th>Price</th>
				<th>Time</th>
			</tr>
			</thead>
			<tbody>
			{mapItems(trades, Tr)}
			</tbody>
		</table>
	)
}

TradesHistory.propTypes = {}

TradesHistory.defaultProps = {}

export const TradesHistoryTracker = getWithTracker(({ _pair }) => {
	return {
		trades: collections.Trades.find(
			{ _pair },
			{ limit: 10 },
		).fetch(),
	}
})(TradesHistory)

TradesHistoryTracker.propTypes = {}

TradesHistoryTracker.defaultProps = {}