import React from 'react'
import { mapItems, getWithTracker } from '/stereor'
import * as collections from '/collections'
import { price } from '/helpers/price'
import _ from 'lodash'

const Tr = props => {
	const { item, payload } = props
	return (
		<tr className={`${item.change > 0 ? 'buy' : 'sell'} ${item._id === payload._pair ? 'lab-background-active' : ''}`}>
			<td><a href={`/${item._id}`} className="lab-link-text pair-title">{item._id}</a></td>
			<td>{price(item.price)}</td>
			<td>{_.round((100 / item.price) * item.change, 2)}%</td>
		</tr>
	)
}

export const MarketOverview = (props) => {
	const { children, pairs, _pair } = props
	return (
		<table>
			<thead>
			<tr className="pair-title">
				<th>Pair</th>
				<th>Price</th>
				<th>Change</th>
			</tr>
			</thead>
			<tbody>
			{mapItems(pairs, Tr, { _pair })}
			</tbody>
		</table>
	)
}

MarketOverview.propTypes = {}

MarketOverview.defaultProps = {}

export const MarketOverviewTracker = getWithTracker(props => {
	const { _pair } = props
	return {
		pairs: collections.Pairs.find().fetch(),
		_pair,
	}
})(MarketOverview)

