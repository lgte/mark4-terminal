import React from 'react'
import PropTypes from 'prop-types'
import { getWithTracker, mapItems } from '/stereor'
import * as collections from '/collections'

const OrderItem = ({ item }) => {
	return (
		<tr>
			<td>{item._id}</td>
			<td>{item.direction}</td>
			<td>{item.type}</td>
			<td>{item.amount}</td>
			<td>{item.amountLeft}</td>
			<td>{item.price}</td>
		</tr>
	)
}

export const OrderList = ({ orders }) => {
	return (
		<table>
			<tbody>
			<tr>
				<th>id</th>
				<th>direction</th>
				<th>type</th>
				<th>amount</th>
				<th>amount left</th>
				<th>price</th>
			</tr>
			{mapItems(orders, OrderItem)}
			</tbody>
		</table>
	)
}

OrderList.propTypes = {}

OrderList.defaultProps = {}

export const OrderListTracker = getWithTracker(({ _user, _pair }) => {
	const query = { _user, _pair }
	//console.log({ query })
	return {
		orders: collections.Orders.find(query).fetch(),
	}
})(OrderList)

OrderListTracker.propTypes = {}

OrderListTracker.defaultProps = {}