import React from 'react'
import PropTypes from 'prop-types'

import { call, clientUser, getWithTracker, flatten } from '/stereor'
import { WaitFor } from '/components/WaitFor'
import { Link } from '../Link'

const CurrentUserLi = props => {
	const { user } = props
	const flatUser = flatten(user)
	return (
		<>
			<li>
				<img src="/images/svg/user-white.svg" className="lab-icon"/>
				<a href={`/my/profile/${user._id}`}>{flatUser['profile.name'] || flatUser['username']}</a>
			</li>
			<li>
				<img src="/images/svg/user-auth-white.svg?2" className="lab-icon"/>
				<a href={`/auth/0`}>Register Or Switch User</a>
			</li>
		</>
	)
}

export const Header = ({ user }) => {
	const usr = (user || {})
	return (
		<header className="app-header">
			<div className="content">
				<section className="app-header-section">
					<ul className="nav">
						<li>
							<div className="brand">
								<a href="/">
									<img src={'/logo.svg'} style={{ width: '3rem' }}/>
								</a>
							</div>
						</li>
						<li>
							<img src="/images/svg/wallet-white.svg" className="lab-icon"/>
							<a href="/my/accounts">Your Wallets</a>
						</li>
						<li>
							<img src="/images/svg/history-white.svg?1" className="lab-icon"/>
							<a href="/my/orders">Order History</a>
						</li>
						<li style={{ flex: 'auto' }}></li>

						{/**
						<li>
							<a href={'#'} onClick={e => call('devGimmeMoney')}>Dev: Gimme Money</a>
						</li>
						 **/}

						<WaitFor data={{ user: usr }} then={CurrentUserLi} placeholder={<li>loading</li>}/>
					</ul>
				</section>
			</div>
		</header>
	)
}

Header.propTypes = {
	user: PropTypes.object,
}

Header.defaultProps = {
	user: {},
}

export const HeaderTracker = getWithTracker(props => {
	return {
		user: clientUser(),
	}
})(Header)

