import React from 'react'
import { mapItems, getWithTracker, call, subscribe, clientUser } from '/stereor'
import { WaitFor } from '../WaitFor'
import * as collections from '/collections'

export const SplashScreen = (props) => {
	const { children, user } = props
	return (
		<>
			{!user && (
				<div className="splashScreen">
					<div className="splashScreen-content">
						<img src={'/logo.svg'}/>
						<div>loading...</div>
					</div>
				</div>
			)}
		</>
	)
}

export const SplashScreenTracker = getWithTracker(props => {
	return {
		user: clientUser(),
	}
}, props => {
	return []
})(SplashScreen)

