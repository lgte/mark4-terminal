import React from 'react'
import PropTypes from 'prop-types'
import { mapItems } from '/stereor'
import _ from 'lodash'


export const WaitForData = (props) => {
	let { children, ready, waitFor } = props

	!waitFor && (waitFor = ready)
	console.log('waitForData')

	const bools = waitFor.map(item => {
		if (_.isObject(item)) {
			return Object.keys(item).length > 0
		}

		if (_.isArray(item)) {
			return item.length > 0
		}

		return item !== undefined
	})

	console.log( bools, waitFor )

	return (
		<>
			{bools.indexOf(false) != -1 ? 'loading' : children}
		</>
	)
}

WaitForData.propTypes = {}

WaitForData.defaultProps = {}

