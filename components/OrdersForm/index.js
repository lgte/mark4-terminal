import React, { useState } from 'react'
import * as consts from '/constants'
import * as collections from '/collections'
import { call, getWithTracker } from '/stereor'
import { Bus } from '/stereor/Bus'

export const OrdersForm = ({ pair }) => {

	check(pair, Object)

	const [values, setValues] = useState({
		amountBase: 0,
		price: pair.price,
		type: consts.ORDER_TYPES.LIMIT,
		direction: consts.ORDER_DIRECTIONS.SELL,
		_pair: pair._id,
	})

	Bus.sub('orderBookClick', payload => {
		setValues({
			...values,
			price: payload.price
		})
	})

	if (pair.price !== undefined && values.price === -1) {
		setValues({
			...values,
			price: pair.price,
			_pair: pair._id,
		})
	}

	const fieldProps = name => ({
		name,
		value: values[name],
		onChange: e => {
			setValues({
				...values,
				[e.target.name]: e.target.type === 'number'
					? +e.target.value
					: e.target.value,
			})
		},
	})

	const onSubmit = e => {
		e.preventDefault()
		call('ordersInsert', values)
	}

	const onClick = e => {
		setValues({ ...values, [[e.target.name]]: e.target.value })
	}

	return (
		<>
			<div className="order-forms">
				<form className={`order-form order-form--${values.type}`} onSubmit={onSubmit}>
					<label>Type</label>
					<select {...fieldProps('type')}>
						<option value={consts.ORDER_TYPES.LIMIT}>Limit</option>
						<option value={consts.ORDER_TYPES.STOP}>Stop</option>
						<option value={consts.ORDER_TYPES.MARKET}>Market</option>
					</select>

					<label>Price</label>
					<input
						type="number"
						{...fieldProps('price')}
						disabled={values.type === consts.ORDER_TYPES.MARKET}
					/>

					<label><span className={'pair-title'}>Amount {pair._tokenBase}</span></label>
					<input
						type="number"
						step="0.00000001"
						{...fieldProps('amountBase')}
					/>
					<a href="">25%</a>,
					<a href="">50%</a>,
					<a href="">75%</a>,
					<a href="">100%</a>

					<input type="hidden" {...fieldProps('_pair')} />
					<button
						className="lab-background-plus"
						type="submit"
						name={'direction'}
						value={'buy'}
						onClick={onClick}>Buy <span className={'pair-title'}>{pair._tokenBase}</span></button>
				</form>
				<form className="order-form" onSubmit={onSubmit}>
					<label>Type</label>
					<select {...fieldProps('type')}>
						<option value={consts.ORDER_TYPES.LIMIT}>Limit</option>
						<option value={consts.ORDER_TYPES.STOP}>Stop</option>
						<option value={consts.ORDER_TYPES.MARKET}>Market</option>
					</select>

					<label>Price</label>
					<input type="number" {...fieldProps('price')} disabled={values.type === consts.ORDER_TYPES.MARKET}/>

					<label><span className={'pair-title'}>amount {pair._tokenBase}</span></label>
					<input type="number" step="0.00000001" {...fieldProps('amountBase')} />
					<a href="">25%</a>,
					<a href="">50%</a>,
					<a href="">75%</a>,
					<a href="">100%</a>

					<input type="hidden" {...fieldProps('_pair')} />
					<button
						className="lab-background-minus"
						type="submit"
						name={'direction'} value={'sell'} onClick={onClick}>Sell <span
						className={'pair-title'}>{pair._tokenBase}</span></button>
				</form>
			</div>
			<div className="content">
				Quoted amount: ~ {pair.price * values.amountBase} <span className="pair-title">{pair._tokenQuoted}</span>
			</div>
		</>
	)
}

export const OrdersFormTracker = getWithTracker(({ _pair }) => {
	return {
		pair: collections.Pairs.findOne(_pair) || {},
	}
}, props => {
	return []
})(OrdersForm)

