import React from 'react'

import { getWithTracker } from '/stereor'
import { mapItems } from '/stereor'
import * as collections from '/collections'
import { price } from '/helpers/price'
import { Bus } from '/stereor/Bus'

const Tr = props => {
	const { item } = props
	const onClick = e => {
		Bus.pub('orderBookClick', item)
	}
	return (
		<tr className={`lab-text-${item.direction === 'sell' ? 'minus' : 'plus'}`} onClick={onClick}>
			<td>{price(item.price, 4)}</td>
			<td>{price(item.amountBaseLeft, 4)}</td>
			<td>{price(item.amountQuoted, 4)}</td>
		</tr>
	)
}

export const OrderBook = ({ orderBook, pair }) => {
	return (
		<div className="widget flex-auto order-book" style={{ paddingBottom: '1rem' }}>
			<div className="order-book-tableContainer">
				<table className="order-book-table">
					<tbody>
					<tr className="pair-title">
						<th>Price {pair._tokenBase}</th>
						<th>Amount</th>
						<th>Quote</th>
					</tr>
					{mapItems(orderBook.sell, Tr)}
					{mapItems(orderBook.buy, Tr)}
					</tbody>
				</table>
			</div>
		</div>
	)
}

OrderBook.propTypes = {}

OrderBook.defaultProps = {}

export const OrderBookTracker = getWithTracker(({ _pair }) => {
	return {
		pair: collections.Pairs.findOneOrDie(_pair),
		orderBook: collections.OrderBooks.findOne({ _pair }) || { _pair, sell: [], buy: [] },
	}
})(OrderBook)

OrderBookTracker.propTypes = {}

OrderBookTracker.defaultProps = {}