import React, { useState } from 'react'
import { mapItems, getWithTracker, call, clientUserId } from '/stereor'

export const PaymentCode = ({ _token }) => {

	check(_token, String)

	const [wallet, setWallet] = useState(null)
	const [payment, setPayment] = useState(null)

	if (wallet && payment) {
		return (
			<div className="widget">
				<div className="widget-title">Deposit</div>
				<div className="content">
					<div>Your transaction is processing</div>
					<div>Send your {_token} to {wallet.address}</div>
					<div>{wallet.destinationTag}</div>
					<div>deposit id: {payment}</div>
				</div>
			</div>
		)
	}
	return (
		<div className="widget">
			<div className="widget-title">Deposit</div>
			<div className="content">
				Payment could be precessed up to 2 hours depending on
				<span className="pair-title">{_token}</span> blockchain conditions and working hours for larger amounts
			</div>
			<div className="content">
				<button type="submit" onClick={e => {
					e.preventDefault()
					call('walletsGenerate', _token)
						.then(wallet => {
							setWallet(wallet)
							call('depositsInsert', {
								_token,
								_wallet: wallet._id,
							}).then(payment => setPayment(payment))
						})
				}}>
					Get Address
				</button>
			</div>
		</div>
	)
}

export const PaymentCodeTracker = getWithTracker(({ _token }) => {
	return {
		_token,
	}
}, props => {
	return []
})(PaymentCode)

