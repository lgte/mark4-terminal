import React, { useEffect, useState } from 'react'
import { mapItems, getWithTracker, call, subscribe } from '/stereor'
import * as collections from '/collections'

const Li = props => {
	const { item } = props

	const [animation, setAnimation] = useState('')

	useEffect(() => {
		setTimeout(() => {
			call('notificationsMarkAsShown', item._id)
		}, 5500)
	})

	return (
		<li className={`notifications-notification notifications-notification--${item.type} animated fadeIn ${animation}`}>
			<p className="notification-title">{item.title}</p>
			<p className="notification-description">{item.description}</p>
		</li>
	)
}

export const Notifications = (props) => {
	const { children, notifications } = props
	return (
		<ul className="notifications">
			{mapItems(notifications, Li)}
		</ul>
	)
}

export const NotificationsTracker = getWithTracker(props => {
	return {
		notifications: collections.Notifications.find().fetch()
	}
}, props => {
	return []
})(Notifications)

