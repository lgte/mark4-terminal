import React, { useState, useEffect } from 'react'
import { Tracker } from 'meteor/tracker'
import { getWithTracker, call } from '/stereor'
import { Collections } from '/collections'

export const Chart = ({ _pair }) => {

	check(_pair, String)

	const ref = React.createRef()

	useEffect(() => {

		call('candlesFind', { _pair, timeFrame: 'minute' }, { sort: { timestamp: 1 } })
			.then(candles => {
				// split the data set into ohlc and volume
				const data = candles.map(candle => {
					return [
						candle.timestamp,
						candle.open,
						candle.high,
						candle.low,
						candle.close,
						candle.volume,
					]
				})
				let ohlc = [],
					volume = [],
					dataLength = data.length,
					i = 0

				for (i; i < dataLength; i += 1) {
					ohlc.push([
						data[i][0], // the date
						data[i][1], // open
						data[i][2], // high
						data[i][3], // low
						data[i][4], // close
					])

					volume.push([
						data[i][0], // the date
						data[i][5], // the volume
					])
				}

				Highcharts.stockChart('chart', {
					chart: {
						events: {
							load: function() {
								let init = false

								Collections.Candles.find({ _pair, timeFrame: 'minute' }, { sort: { timestamp: 1 } }).observe({
									added: (candle) => {
										if (!init) {
											return
										}
										const series = this.series[0]
										series.addPoint([
											candle.timestamp,
											candle.open,
											candle.high,
											candle.low,
											candle.close,
										], true, true)

										const series2 = this.series[1]
										series2.addPoint([
											candle.timestamp,
											candle.volume,
										], true, true)
									},
									changed: (candle) => {
										if (!init) {
											return
										}
										const series = this.series[0]
										series.addPoint([
											candle.timestamp,
											candle.open,
											candle.high,
											candle.low,
											candle.close,
										], true, true)

										const series2 = this.series[1]
										series2.addPoint([candle.timestamp, candle.volume], true, true)
									},
								})
								init = true
							},
						},
					},
					time: {
						useUTC: true,
					},
					rangeSelector: {
						buttons: [{
							count: 100,
							type: 'minute',
							text: '5M',
							dataGrouping: {
								forced: false,
								units: [
									['minute', [5]],
								],
							},
						}, {
							count: 100,
							type: 'minute',
							text: '15M',
							dataGrouping: {
								forced: false,
								units: [
									['minute', [15]],
								],
							},
						}, {
							count: 100,
							type: 'minute',
							text: '30M',
							dataGrouping: {
								forced: false,
								units: [
									['minute', [30]],
								],
							},
						}, {
							count: 100,
							type: 'hour',
							text: '1H',
							dataGrouping: {
								forced: false,
								units: [
									['hour', [1]],
								],
							},
						}, {
							count: 100,
							type: 'hour',
							text: '4H',
							dataGrouping: {
								forced: false,
								units: [
									['hour', [4]],
								],
							},
						}, {
							count: 100,
							type: 'day',
							text: '1D',
							dataGrouping: {
								forced: false,
								units: [
									['day', [1]],
								],
							},
						}],
						inputEnabled: false,
						selected: 0,
					},

					yAxis: [{
						labels: {
							align: 'left',
						},
						height: '80%',
						resize: {
							enabled: true,
						},
					}, {
						labels: {
							align: 'left',
						},
						top: '80%',
						height: '20%',
						offset: 0,
					}],

					tooltip: {
						shape: 'square',
						headerShape: 'callout',
						borderWidth: 0,
						shadow: false,
						positioner: function(width, height, point) {
							var chart = this.chart,
								position

							if (point.isHeader) {
								position = {
									x: Math.max(
										// Left side limit
										chart.plotLeft,
										Math.min(
											point.plotX + chart.plotLeft - width / 2,
											// Right side limit
											chart.chartWidth - width - chart.marginRight,
										),
									),
									y: point.plotY,
								}
							} else {
								position = {
									x: point.series.chart.plotLeft,
									y: point.series.yAxis.top - chart.plotTop,
								}
							}

							return position
						},
					},
					series: [{
						type: 'candlestick',
						id: _pair,
						name: _pair.toUpperCase(),
						data: ohlc,
						upLineColor: '#2ac12a',
						lineColor: '#F60070',
						color: '#F60070',
						upColor: '#2ac12a',
					}, {
						type: 'column',
						id: _pair + '-volume',
						name: _pair.toUpperCase() + ' volume',
						data: volume,
						yAxis: 1,
					}],
					responsive: {
						rules: [{
							condition: {
								maxWidth: 800,
							},
							chartOptions: {
								rangeSelector: {
									inputEnabled: false,
								},
							},
						}],
					},
				})


			})

	})


	return (
		<div>
			<div ref={ref} id={'chart'}></div>
		</div>
	)
}

export const ChartTracker = getWithTracker(({ _pair }) => {
	return {
		//pair: collections.Pairs.findOneOrDie(_pair)
		_pair,
	}
}, props => {
	return []
})(Chart)

