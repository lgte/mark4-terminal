import React from 'react'
import { mapItems, getWithTracker, call, subscribe } from '/stereor'
import * as collections from '/collections'
import { WaitFor } from '../WaitFor'

export const HeaderAdmin = (props) => {
	const { children } = props
	return (
		<header className="app-header">
			<div className="content">
				<section className="app-header-section">
					<ul className="nav">
						<li>
							<div className="brand">
								<a href="/btceth">
									<img src={'https://my.btcnext.io/assets/images/logo.svg'} style={{ width: '3rem' }}/>
								</a>
							</div>
						</li>
						<li>
							<a href="/my/accounts">Your Wallets</a>
						</li>
						<li>
							<a href="/my/orders">Your Orders History</a>
						</li>
						<li style={{ flex: 'auto' }}></li>
					</ul>
				</section>
			</div>
		</header>
	)
}

