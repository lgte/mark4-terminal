import React from 'react'
import PropTypes from 'prop-types'
import { mapItems } from '/stereor'
import _ from 'lodash'

export const WaitFor = (props) => {
	const { children, data, then, placeholder } = props
	const Com = then

	const bools = Object.keys(data).map(key => {
		const item = data[key]

		if (_.isObject(item)) {
			return Object.keys(item).length > 0
		}

		if (_.isArray(item)) {
			return item.length > 0
		}

		return item !== undefined
	})

	return (
		<>
			{bools.indexOf(false) != -1 ? placeholder || 'loading' : <Com {...data}/>}
		</>
	)
}