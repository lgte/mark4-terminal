import React from 'react'
import PropTypes from 'prop-types'
import { getWithTracker } from '/mightyor/components/utils'

export const TransactionList = ({}) => {
	return (
		<div>TransactionList</div>
	)
}

TransactionList.propTypes = {}

TransactionList.defaultProps = {}

export const TransactionListTracker = getWithTracker(({}) => {
	return {}
})(TransactionList)

TransactionListTracker.propTypes = {}

TransactionListTracker.defaultProps = {}