import React, { useState } from 'react'
import { PagesRoutes } from '/stereor/components/PagesRoutes'

import { HeaderTracker } from '/components/Header'
import { Footer } from '/components/Footer'
import { NotificationsTracker } from '../Notifications'
import { SplashScreenTracker } from '../SplashScreen'

const App = ({ location }) => {
	return (
		<>
			<div className="app">
				<SplashScreenTracker/>
				<NotificationsTracker/>
				<HeaderTracker/>
				<main className="app-main">
					<PagesRoutes location={location}/>
				</main>
				<Footer/>
			</div>
		</>
	)
}

export { App }
