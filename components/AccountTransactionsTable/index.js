import React from 'react'
import { mapItems, getWithTracker, call, subscribe } from '/stereor'
import * as collections from '/collections'
import { dateTime } from '/helpers/dateTime'

export const AccountTransactionsTable = (props) => {
	const { children, transactions } = props

	return (
		<table>
			<thead>
			<tr>
				<th>ID</th>
				<th>DateTime</th>
				<th>Form</th>
				<th>To</th>
				<th>Amount</th>
			</tr>
			</thead>
			<tbody>
			{mapItems(transactions, ({item}) =>(
				<tr>
					<td>{item._id}</td>
					<td>{dateTime(item.createdAt)}</td>
					<td>{item._accountDebit}</td>
					<td>{item._accountCredit}</td>
					<td>{item.amount}</td>
				</tr>
			))}
			</tbody>
		</table>
	)
}

export const AccountTransactionsTableTracker = getWithTracker(props => {
	const { _account } = props
	const query = { $or: [{ _accountDebit: _account }, { _accountCredit: _account }] }
	subscribe('magic2', [
		['Transactions', query],
	])
	return {
		transactions: collections.Transactions.find(query).fetch(),
	}
}, props => {
	return []
})(AccountTransactionsTable)

