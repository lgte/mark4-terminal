import React from 'react'
import { mapItems, getWithTracker, call, subscribe, clientUserId } from '/stereor'
import * as collections from '/collections'
import { OrdersTable } from '../OrdersTable'

export const MyOrders = (props) => {
	const { children, orders } = props
	return (
		<>
			<div className="widget-title">Active Orders</div>
			<OrdersTable orders={orders.filter(order => !order.flags.filled && !order.flags.cancelled)}/>

			<div><br/></div>
			<div className="widget-title">Orders History</div>
			<OrdersTable orders={orders.filter(order => order.flags.filled)}/>

			<div><br/></div>
			<div className="widget-title">Orders Cancelled</div>
			<OrdersTable orders={orders.filter(order => order.flags.cancelled)}/>
		</>
	)
}

export const MyOrdersTracker = getWithTracker(({_pair}) => {
	return {
		orders: collections.Orders.find({ _user: clientUserId(), _pair }).fetch(),
	}
}, props => {
	return []
})(MyOrders)