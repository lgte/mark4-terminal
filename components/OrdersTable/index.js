import React from 'react'

import { call, mapItems } from '/stereor'
import { dateTime } from '/helpers/dateTime'

const Tr = props => {
	const { item } = props
	return (
		<tr>
			<td style={{width: '10rem'}}>{dateTime(item.createdAt)}</td>
			<td style={{width: '10rem'}}className="pair-title">{item._pair}</td>
			<td style={{width: '10rem'}}>{item.type}</td>
			<td style={{width: '10rem'}}>{item.direction}</td>
			<td style={{width: '10rem'}}>{item.amountBase}</td>
			<td style={{width: '10rem'}}>{item.amountBaseLeft}</td>
			<td style={{width: '10rem'}}>{item.price}</td>
			<td style={{width: '10rem'}}>{item.amountQuoted}</td>
			<td style={{textAlign: 'right'}}>
				{!item.flags.cancelled && !item.flags.filled && (
					<a href={'#'} onClick={e => {e.preventDefault(); call('ordersCancel', item._id)}}>Cancel</a>
				)}
			</td>
		</tr>
	)
}

export const OrdersTable = (props) => {
	const { children, orders } = props
	return (
		<table>
			<thead>
			<tr>
				<th>Date</th>
				<th>Pair</th>
				<th>Type</th>
				<th>Side</th>
				<th>Amount</th>
				<th>Amount Left</th>
				<th>Price</th>
				<th>Amount Quoted</th>
				<th></th>
			</tr>
			</thead>
			<tbody>
			{mapItems(orders, Tr)}
			</tbody>
		</table>
	)
}

OrdersTable.propTypes = {}

OrdersTable.defaultProps = {}

