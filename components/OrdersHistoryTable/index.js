import React from 'react'
import { subscribe, getWithTracker } from '/stereor'
import { OrderList } from '../OrderList'

import * as collections from '/collections'

export const OrdersHistoryTable = (props) => {
	const { children, orders } = props
	return (
		<>
			<OrderList orders={orders}/>
		</>
	)
}

export const OrdersHistoryTableTracker = getWithTracker(props => {
	return {
		orders: collections.Orders.find().fetch()
	}
}, props => {
	return []
})(OrdersHistoryTable)

