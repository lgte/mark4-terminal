import React from 'react'
import { getForm, call } from '/stereor'

export const AuthRegistrationForm = (props) => {
	const { } = props
	const form = getForm()
	return (
		<>
			<form {...form.getFormAttributes(e => {
				const values = e.target.value
				call('usersInsert', {
					email: values.email,
					password: values.password,
					profile: {
						name: values.name
					}
				})
					.then(_user => {
						Meteor.loginWithPassword(values.email, values.password)
					})

			})}>
				<label>Full Name</label>
				<input
					required={true}
					{...form.getInputAttributes('name')} />
				<label>Email</label>
				<input
					required={true}
					{...form.getInputAttributes('email')} />
				<label>Password</label>
				<input
					required={true}
					type="password"
					{...form.getInputAttributes('password')} />
				<button type="submit">Register</button>
			</form>
		</>
	)
}

