import React from 'react'
import { mapItems, getWithTracker, call, subscribe, clientUserId } from '/stereor'
import * as collections from '/collections'

import { WaitFor } from '../WaitFor'
import { formatPrice } from '../../helpers/price'

export const BalancesWidget = (props) => {
	const { children, accountBase, accountQuoted } = props
	return (
		<div className="widget flex-fixed">
			<div className="widget-title">Balances</div>
				<table className="">
					<tbody>
						<tr>
							<td>{formatPrice(accountBase.balance)}<span className="pair-title">{accountBase._token}</span></td>
							<td style={{width: '5rem'}}>
								<a href={`/my/accounts/${accountBase._token}/deposit`} className="lab-link-text">
									<img src="/images/svg/deposit.svg" className="lab-icon"/>
								</a>
								&nbsp;
								<a href={`/my/accounts/${accountBase._token}/withdraw`} className="lab-link-text">
									<img src="/images/svg/withdraw.svg" className="lab-icon"/>
								</a>
							</td>
						</tr>
						<tr>
							<td>{formatPrice(accountQuoted.balance)}<span className="pair-title">{accountQuoted._token}</span></td>
							<td style={{width: '5rem'}}>
								<a href={`/my/accounts/${accountQuoted._token}/deposit`} className="lab-link-text">
									<img src="/images/svg/deposit.svg" className="lab-icon"/>
								</a>
								&nbsp;
								<a href={`/my/accounts/${accountQuoted._token}/withdraw`} className="lab-link-text">
									<img src="/images/svg/withdraw.svg" className="lab-icon"/>
								</a>
							</td>
						</tr>
					</tbody>
				</table>
		</div>
	)
}

export const BalancesWidgetTracker = getWithTracker(props => {
	const pair = collections.Pairs.findOneOrDie(props._pair)
	const accountBase = collections.Accounts.findOne({
		_user: Meteor.userId(),
		_token: pair._tokenBase
	})

	const accountQuoted = collections.Accounts.findOne({
		_user: Meteor.userId(),
		_token: pair._tokenQuoted
	})

	return {
		accountBase,
		accountQuoted,
	}
}, props => {
	return []
})(BalancesWidget)
