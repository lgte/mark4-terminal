import React from 'react'
import { StaticRouter , BrowserRouter, Route, Link as L } from 'react-router-dom'

export const Link = props => {
	return Meteor.isServer ? <StaticRouter><L to={props.to}>{props.children}</L></StaticRouter> : <BrowserRouter><L to={props.to}>{props.children}</L></BrowserRouter>
}