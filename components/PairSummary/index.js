import React from 'react'
import { mapItems, getWithTracker, subscribe, clientUserId } from '/stereor'
import { formatPrice, price } from '/helpers/price'

import * as collections from '/collections'
import _ from 'lodash'

export const PairSummary = (props) => {
	const { candle } = props

	return (
		<ul className="summary">
			<li>
				<span className="summary-pair-title">{props.pair._id}</span>
			</li>
			<li>
				<span className={`summary-value lab-text-${candle.open < candle.close ? 'minus' : 'plus'}`}>
					{formatPrice(candle.close)}&nbsp;
					<span className="pair-title">{props.pair._tokenBase}</span>
				</span>
				<span className="summary-title">Last Price</span>
			</li>
			<li>
				<span
					className={`summary-value ${candle.open - candle.close > 0 ? 'buy' : 'sell'}`}>{_.round(100 / candle.close * (candle.open - candle.close), 2)}%</span>
				<span className="summary-title">24h Change</span>
			</li>
			<li>
				<span className="summary-value">{price(candle.high)}</span>
				<span className="summary-title">24h High</span>
			</li>
			<li>
				<span className="summary-value">{price(candle.low)}</span>
				<span className="summary-title">24h Low</span>
			</li>
			<li>
				<span className="summary-value">{price(candle.volume)}</span>
				<span className="summary-title">24h Volume</span>
			</li>
		</ul>
	)
}

export const PairSummaryTracker = getWithTracker(props => {
	subscribe('magic2', [
		['Candles', { _pair: props._pair, timeFrame: 'day' }],
	])
	const pair = collections.Pairs.findOneOrDie(props._pair)
	const candle = collections.Candles
		.findOne({ timeFrame: 'day', _pair: props._pair }) || { open: 0, high: 0, close: 0, low: 0, volume: 0 }
	return {
		candle,
		pair
	}
})(PairSummary)
