import React, { useState } from 'react'
import { call, getForm } from '/stereor'

export const WithdrawForm = props => {
	const { _token } = props
	check(_token, String)

	const [state, setState] = useState({
		submitted: false
	})

	const f = getForm({
		address: '',
		destinationTag: '',
		amount: 0,
	})

	if (state.submitted) {
		return (
			<div>your request was submitted</div>
		)
	}

	return (
		<form {...f.getFormAttributes(e => {
			call('withdrawalsInsert', {
				wallet: {
					address: e.target.value.address,
					destinationTag: e.target.value.destinationTag,
				},
				amount: e.target.value.amount,
				_token,
			})
			setState({
				submitted: true
			})
		})}>
			<label>Address</label>
			<input
				type="text"
				{...f.getInputAttributes('address')} />
			<label>Memo or Destination Tag(optional)</label>
			<input
				type="text"
				{...f.getInputAttributes('destinationTag')} />
			<label>Amount</label>
			<input
				type="number"
				{...f.getInputAttributes('amount')} />

			<button
				type={'submit'}>Create Withdraw Request</button>
		</form>
	)
}