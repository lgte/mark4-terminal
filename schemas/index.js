export const id = () => {
	return {
		type: String
	}
}

export const datedAt = () => ({
	type: Date,
	autoValue() {
		return new Date()
	},
})

export const user = () => ({
	type: String,
	optional: true,
	autoValue() {
		return Meteor.userId()
	},
})

export const boolean = (defaultValue = false) => ({
	type: Boolean,
	defaultValue
})

export const string = (defaultValue = '')  => ({
	type: String,
	defaultValue
})

export const flags = (defaultValue = {}) => {
	const schema = {
		flags: {
			type: Object,
			defaultValue
		}
	}
	Object.keys(defaultValue).forEach(key => schema[`flags.${key}`] = Boolean)
	return schema
}