import { Queue } from '/stereor/Queue'
import { Lib } from '/lib'
import { syncHitbtcTickers } from '/intervals/syncHitbtcTickers'
import { run } from '/stereor/server/run'
import { App } from '/components/App'

export const systemStart = Queue.createTask(async () => {
	await Lib.systemStartInterval(syncHitbtcTickers)
	await run(App)
}, 'systemStart')
