import { Queue } from '/stereor/Queue'
import { Lib } from '/lib'

/**
 * Базовая валюта – это та валюта в конкретной валютной паре, цена одной единицы которой всегда меряется в
 * единицах другой (котируемой) валюты.
 *
 * Котируемая валюта – это та валюта, в единицах которой выражается цена одной единицы базовой валюты.
 * Базовая валюта в обозначении валютной пары пишется первой, а котируемая – второй.
 */

// на примере ETHBTC
// продаем или покупаем ETH за BTC
// amount всегда в базовой
// amountQuoted всегда в валюте котировки
// цена всегда указывается в валюте котировки

export const ordersInsert = Queue.createTask(
	(order) => {
		check(order, Object)
		try {
			return Lib.ordersInsert(order)
		} catch (e) {
			console.error(e)
			return false
		}
	},
	'ordersInsert',
)