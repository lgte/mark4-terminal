import { Queue } from '/stereor/Queue'
import { Lib } from '/lib'

export const ordersCancel = Queue.createTask(
	(_order) => {
		check(_order, String)
		try {
			Lib.ordersCancel(_order)
			return _order
		} catch (e) {
			console.error(e)
			return false
		}

	}
)