import { Lib } from '/lib'
import { Queue } from '/stereor/Queue'

export const withdrawalsConfirm = Queue.createTask(function(_withdrawal) {
	check(_withdrawal, String)
	Lib.withdrawalsConfirm(_withdrawal)
}, 'withdrawalsConfirm')