import { Lib } from '/lib'
import { Queue } from '/stereor/Queue'

export const robotsTopUp = Queue.createTask(function(username, _token, amount) {
	check(_token, String)
	check(amount, Number)
	check(username, String)

	Lib.transactionsTopupRobot(
		username,
		_token,
		amount
	)


}, 'depositsConfirm')