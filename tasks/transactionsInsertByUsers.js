import { Queue } from '/stereor/Queue'
import { Lib } from '/lib'

export const transactionsInsertByUsers = Queue.createTask(async (transaction) => {
	Lib.transactionInsertUser2User(transaction)
}, 'transactionsInsertByUsers')