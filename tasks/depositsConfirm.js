import { Lib } from '/lib'
import { Queue } from '/stereor/Queue'

export const depositsConfirm = Queue.createTask(function(_deposit, amount) {
	check(_deposit, String)
	check(amount, Number)
	Lib.depositsConfirm(_deposit, amount)
}, 'depositsConfirm')