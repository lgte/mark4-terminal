import { Queue } from '/stereor/Queue'
import { systemReset } from '/stereor/server/systemReset'

import { Lib } from '/lib'

import * as initializationData from '/data/initialization'

export const systemInitialize = Queue.createTask((reset = false) => {
	reset && systemReset()
	initializationData.Users.map(Lib.usersInsert)
	initializationData.Tokens.map(Lib.tokensInsert)
	initializationData.Pairs.map(Lib.pairsInsert)
}, 'initialize')