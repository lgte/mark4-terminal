import { App } from '/components/App'
import { subscribe, usersInsertAnonymous } from '/stereor'
import { run } from '/stereor/client/run'

(() => {
	Meteor.startup(async() => {
		await usersInsertAnonymous('usersInsert')

		await subscribe('magic2', [
				['Pairs'],
				['OrderBooks'],
				['Trades'],
				['Orders', {}, { limit: 500 }],
				['Accounts'],
				['PaymentsAdapters'],
				['Payments'],
				['Notifications', { 'flags.shown': false }],
			],
		)
		run(App)
	})
})()