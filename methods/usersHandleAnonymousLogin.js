import { call, getObjectId, Method } from '/stereor'
import { Lib } from '/lib'

export const usersHandleAnonymousLogin = new Method(function() {
	if (Meteor.userId() === null) {
		const objectId = getObjectId()
		const profile = {
			name: 'Anonymous',
			anonymous: true,
		}
		const _user = Lib.usersInsert({
			username: objectId,
			password: objectId,
			profile,
		})
		console.log(_user)
		this.setUserId(_user)
		return _user
	}
	return Meteor.userId()
})