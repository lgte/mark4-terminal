import { Collections } from '/collections'
import { Method } from '/stereor'

export const accountsFindOne = new Method(function(_token) {
	const _user = Meteor.userId()
	return Collections.Accounts.findOneOrDie({
		_user,
		_token
	})
})