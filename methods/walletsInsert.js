import { Method } from '/stereor'
import { Collections } from '/collections'

export const walletsInsert = new Method(async function(wallet) {
	check(wallet, Object)
	return Collections.Wallets.insert(wallet)
})