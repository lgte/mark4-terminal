import { Method } from '../stereor'
import * as collections from '../collections'

export const notificationInsert = new Method(function(notification) {
	notification._user = Meteor.userId()
	collections.Notifications.insert(notification)
})
