import { Method } from '/stereor'
import { Tasks } from '/tasks'
import { Lib } from '/lib'
import { Constants } from '/constants'

export const devGimmeMoney = new Method(function() {

	const startedtAt = new Date().getTime()
	Lib.tokensFind().forEach(token => Tasks.transactionsInsertByUsers({
			_userCredit: 'issuer',
			_userDebit: [Meteor.userId()],
			_token: token._id,
			amount: 100,
		}),
	)

	console.warn('devGimmeMoney', (new Date().getTime() - startedtAt) / 1000)

	Lib.logsInsert({
		group: Constants.LOG_GROUP.SYSTEM,
		level: 'message',
		message: `devGimmeMoney` + ((new Date().getTime() - startedtAt) / 1000)
	})

})