import { Method } from '/stereor'
import { Lib } from '/lib'
import { Accounts } from 'meteor/accounts-base'

export const usersInsertOrLogin = new Method(function(data) {
	check(data, Object)
	try {
		const user = Accounts.findUserByUsername(data.username) || data
		user._id = user._id || Lib.usersInsert(user)
		return user
	} catch (e) {
		console.error(e)
	}

})