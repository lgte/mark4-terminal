import { Method } from '/stereor'
import * as collections from '/collections'

export const notificationsMarkAsShown = new Method(function(_notification) {
	collections.Notifications.update(_notification, { $set: { 'flags.shown': true } })
})
