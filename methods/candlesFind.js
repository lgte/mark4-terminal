import { Method } from '/stereor'
import { Lib } from '/lib'

export const candlesFind = new Method(function(selector = {}, options = {}) {
	return Lib.candlesFind(selector, options).fetch()
})