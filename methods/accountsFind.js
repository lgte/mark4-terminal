import { Method } from '/stereor'
import { Lib } from '/lib'

export const accountsFind = new Method(function(selector = {}) {
	selector._user = Meteor.userId()
	check(selector._user, String)
	return Lib.accountsFind(selector).fetch()

})