import { Method } from '/stereor'
import { Logs } from '/collections'

export const logsInsert = new Method(function(log) {
	return Logs.insert(log)
})