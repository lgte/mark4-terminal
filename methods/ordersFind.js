import { Method } from '/stereor'
import { Lib } from '/lib'

export const ordersFind = new Method(function(selector = {}) {
	selector._user = Meteor.userId()
	check(selector._user, String)
	return Lib.ordersFind(selector).fetch()
})