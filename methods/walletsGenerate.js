import { Method } from '/stereor'
import {walletsUse} from '/lib/walletsUse'

export const walletsGenerate = new Method(async function(_token) {
	check(_token, String)
	console.log(_token)
	const wallet = walletsUse(Meteor.userId(), _token)
	console.log(wallet)
	return wallet
})