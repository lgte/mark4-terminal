import { Method } from '/stereor'
import { Tasks } from '/tasks'

export const depositsConfirm = new Method(function({ _deposit, amount }) {
	check(_deposit, String)
	check(amount, Number)
	return Tasks.depositsConfirm(_deposit, amount)
})