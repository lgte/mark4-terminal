import { Method } from '/stereor'
import { Lib } from '/lib'

export const withdrawalsInsert = new Method(function(withdrawal) {
	check(withdrawal, Object)
	withdrawal._user = Meteor.userId()
	return Lib.withdrawalsInsert(withdrawal)
})
