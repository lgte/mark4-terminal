import { Method } from '/stereor'
import { Tasks } from '/tasks'

export const ordersCancel = new Method(function(_order) {
	check(_order, String)
	Tasks.ordersCancel(_order)
})