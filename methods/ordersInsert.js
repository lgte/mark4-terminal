import { Method } from '/stereor'
import { Tasks } from '/tasks'

export const ordersInsert = new Method(function(order) {
	order._user = Meteor.userId()
	check(order, Object)
	check(order._user, String)
	return Tasks.ordersInsert(order)
})
