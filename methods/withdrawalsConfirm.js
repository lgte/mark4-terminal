import { Method } from '/stereor'
import { Tasks } from '/tasks'

export const withdrawalsConfirm = new Method(function(_withdrawal) {
	check(_withdrawal, String)
	return Tasks.withdrawalsConfirm(_withdrawal)
})