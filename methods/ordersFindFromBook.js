import { Method } from '/stereor'
import { Lib } from '/lib'

export const ordersFindFromBook = new Method(function(selector = {}) {
	return Lib.ordersFindActive(selector).fetch()
})