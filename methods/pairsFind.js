import { Method } from '/stereor'
import * as collections from '/collections'

export const pairsFind = new Method(function(query = {}) {
	const pairs = collections.Pairs.find(query).fetch()
	return pairs
})
