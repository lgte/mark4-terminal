import { Method } from '/stereor'
import { Tasks } from '/tasks'
import { Lib } from '/lib'
import { Accounts } from 'meteor/accounts-base'

export const usersInsert = new Method(function(data) {
	check(data, Object)
	try {
		const _user = Lib.usersInsert(data)
		Lib.notificationsInsert({
			title: data.username
				? 'You are automatically logged as Anonymous. Start trading right away'
				: 'Registration Complete as ' + Accounts.findUserByEmail(data.email).profile.name,
			_user,
		})
		return _user
	} catch (e) {
		console.warn(e)
		Lib.notificationsInsertByError(e)
	}
})