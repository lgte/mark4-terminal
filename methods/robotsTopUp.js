import { Method } from '/stereor'
import { Tasks } from '/tasks'

export const robotsTopUp = new Method(function(data) {
	check(data, {
		secret: String,
		_token: String,
		username: String,
		amount: Number
	})
	Tasks.robotsTopUp(data.username, data._token, data.amount)
})