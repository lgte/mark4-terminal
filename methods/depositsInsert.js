import { Method } from '/stereor'
import { Lib } from '/lib'

export const depositsInsert = new Method(function({_wallet, _token}) {
	check(_wallet, String)
	check(_token, String)

	const deposit = {
		_wallet,
		_token,
		_user: Meteor.userId()
	}
	return Lib.depositsInsert(deposit)
})