import { Lib } from '/lib'
import { logsInsert } from '/lib/logs'
const hitbtc = require('hitbtc-node-sdk')

export const syncHitbtcTickers = async () => {
	hitbtc.auth(Meteor.settings.hitbtc.key, Meteor.settings.hitbtc.secret)
	const pairs = Lib.pairsFind().fetch()
	const result = await Promise.all(pairs.map(async pair => {
		try {
			return await hitbtc.tickers(pair._id)
		} catch (e) {
			logsInsert({
				group: 'hitbtc',
				level: 'error',
				message: e.message,
				payload: pair
			})
			return null
		}
	}))

	Lib.synchronizationsInsert({
		name: 'hitbtcTickers',
		payload: result
	})
	return result
}