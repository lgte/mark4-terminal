import React from 'react'
import { WithdrawForm } from '/components/WithdrawForm'

export const Withdraw = (props) => {
	const { children, match } = props
	check(match.params._token, String)
	return (
		<div className="content">
			<WithdrawForm _token={match.params._token}/>
		</div>
	)
}