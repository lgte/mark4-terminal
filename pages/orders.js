import React from 'react'
import { mapItems, getWithTracker, call, subscribe } from '/stereor'
import { ProfileForm } from '/components/ProfileForm'

import { OrdersHistoryTableTracker } from '/components/OrdersHistoryTable'

export const Orders = (props) => {
	const { children } = props
	return (
		<div className="content">
			<OrdersHistoryTableTracker/>
		</div>
	)
}
