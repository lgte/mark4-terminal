import React from 'react'
import { mapItems, getWithTracker, call, subscribe } from '/stereor'
import { ProfileForm } from '/components/ProfileForm'

export const Profile = (props) => {
	const { children } = props
	return (
		<div className="content">
			<div className="content-centred">
				<ProfileForm/>
			</div>
		</div>
	)
}

