import React from 'react'
import { clientUser, clientUserId, getWithTracker } from '/stereor'
import { subscribe } from '/stereor'

import { OrdersFormTracker } from '/components/OrdersForm'
import { OrderBookTracker } from '/components/OrderBook'
import { MarketOverviewTracker } from '/components/MarketOverview'
import { ChartTracker } from '/components/Chart'
import { PairSummaryTracker } from '/components/PairSummary'
import { TradesHistoryTracker } from '/components/TradesHistory'
import { MyOrdersTracker } from '/components/MyOrders'
import { BalancesWidgetTracker } from '/components/BalancesWidget'

export const Home = (props) => {
	const { match } = props
	const _pair = match.params._pair
	check(_pair, String)

	subscribe('magic2', [
		['Candles', {
			_pair,
			timeFrame: { $in: ['minute', 'day'] },
		}, {
			limit: 200,
			sort: { timestamp: -1 },
		}],
	])

	return (
		<>
			<div className="content lab-background-lightGray">
				<section className="app-header-section">
					<PairSummaryTracker _pair={_pair}/>
				</section>
			</div>
			<div className="app-workbench">
				<aside className="app-aside">
					<BalancesWidgetTracker _pair={_pair}/>
					<OrderBookTracker _pair={_pair}/>
				</aside>
				<section className="app-section">
					<ChartTracker _pair={_pair}/>
					<div className="widget flex-fixed">
						<div className="widget-title">Place Order</div>
						<OrdersFormTracker _pair={_pair}/>
					</div>
				</section>
				<aside className="app-aside">
					<div className="widget">
						<MarketOverviewTracker _pair={_pair}/>
					</div>
					<div className="widget">
						<div className="widget-title">Trades History</div>
						<TradesHistoryTracker _pair={_pair}/>
					</div>
				</aside>
			</div>
			<div className="widget">
				<MyOrdersTracker _pair={_pair}/>
			</div>
		</>
	)
}

Home.propTypes = {}

Home.defaultProps = {}

export const HomeTracker = getWithTracker(({ match }) => {
	return {
		_pair: match.params._pair,
	}
})(Home)

