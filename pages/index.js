import { React } from 'react'

import { Home } from './home'
import { Accounts, AccountsTracker } from './accounts'
import { Authentication } from './authentication'
import { Deposit } from './deposit'
import { Withdraw } from './withdraw'
import { Profile } from './profile'
import { Orders } from './orders'
import { Transactions } from './transactions'
import { Tools } from './tools'

export default {
	'/': props => {
		if (Meteor.isClient) {
			window.location = '/ethbtc'
		}
		return '/ethbtc'
	},

	'/:_pair': Home,
	'/auth/:_token': Authentication,

	'/my/accounts': AccountsTracker,
	'/my/accounts/:_token/deposit': Deposit,
	'/my/accounts/:_token/withdraw': Withdraw,
	'/my/accounts/:_account/transactions': Transactions,
	'/my/orders': Orders,

	'/my/profile/:_user': Profile,

	'/admin/tools': Tools
}