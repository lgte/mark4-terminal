import React from 'react'
import PropTypes from 'prop-types'
import { mapItems, getWithTracker, subscribe, clientUser, clientUserId } from '/stereor'

import { WaitForData } from '/components/WaitForData'

import * as collections from '/collections'

const Tr = props => {
	const { item } = props
	return (
		<tr>
			<td><a href={`/my/accounts/${item._id}/transactions`} className="pair-title">{item._token}</a></td>
			<td>{item.balance}</td>
			<td>
				[<a href={`/my/accounts/${item._token}/deposit`}>Deposit</a> | <a href={`/my/accounts/${item._token}/withdraw`}>Withdraw</a>]
			</td>
		</tr>
	)
}

export const Accounts = (props) => {
	const { children, accounts } = props
	return (
		<div className="content">
			<WaitForData waitFor={[clientUser()]}>
				<table>
					<thead>
					<tr>
						<th>Token</th>
						<th>Balance</th>
						<th>Actions</th>
					</tr>
					</thead>
					<tbody>
					{mapItems(accounts, Tr)}
					</tbody>
				</table>
			</WaitForData>
		</div>
	)
}

export const AccountsTracker = getWithTracker(props => {
	return {
		accounts: collections.Accounts.find({}).fetch({_user: clientUserId()}),
	}
}, props => {
	return [
		subscribe('magic', { collection: 'Accounts' }),
	]
})(Accounts)

Accounts.propTypes = {}

Accounts.defaultProps = {}

