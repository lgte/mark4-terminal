import React from 'react'
import { ProfileForm } from '/components/ProfileForm'
import { getForm, call } from '/stereor'


const ConfirmDepositForm = props => {

	const form = getForm({
		_deposit: '',
		amount: 0
	})

	return (
		<form {...form.getFormAttributes(e => call('depositsConfirm', e.target.value))}>
			<label>_deposit</label>
			<input type="text" {...form.getInputAttributes('_deposit')}/>
			<label>amount</label>
			<input type="number" {...form.getInputAttributes('amount')}/>
			<button type="submit">confirm</button>
		</form>
	)
}

export const Tools = (props) => {
	const { children } = props
	return (
		<div className="content">
			<div className="content-centred">
				<ConfirmDepositForm/>
			</div>
		</div>
	)
}

