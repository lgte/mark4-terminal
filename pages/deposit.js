import React from 'react'
import { PaymentCodeTracker } from '/components/PaymentCode'

export const Deposit = (props) => {
	const { _token } = props.match.params

	check(_token, String)
	return (
		<div className="content">
			<div className="content-centred">
				<PaymentCodeTracker _token={_token}/>
			</div>
		</div>
	)
}