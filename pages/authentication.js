import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { mapItems, getWithTracker } from '/stereor'
import { AuthLoginForm } from '/components/AuthLoginForm'
import { AuthRegistrationForm } from '/components/AuthRegistrationForm'
import { AuthBadgeFormTracker } from '/components/AuthBadge'

export const Authentication = (props) => {
	const { children } = props

	return (
		<div className="content">
			<div className="content-centred">
				<div className="authentication-forms">
					<div className="authentication-form-container">
						<div className="widget">
							<h2 className={'content'}>Switch User</h2>
							<AuthLoginForm/>
						</div>
					</div>
					<div className="authentication-form-container">
						<div className="widget">
							<h2 className={'content'}>Create User</h2>
							<AuthRegistrationForm/>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export const AuthenticationTracker = getWithTracker(props => {
	return {}
}, props => {
	return []
})(Authentication)

Authentication.propTypes = {}

Authentication.defaultProps = {}

