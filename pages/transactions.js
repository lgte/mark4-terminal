import React, { useEffect, useState } from 'react'
import { AccountTransactionsTableTracker } from '/components/AccountTransactionsTable'

export const Transactions = (props) => {
	const { children, match } = props


	return (
		<div className="content">
			<AccountTransactionsTableTracker _account={match.params._account}/>
		</div>
	)
}

