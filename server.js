require('console.table')

import { startObservers } from './startup/startObservers'
import { Lib } from '/lib'
import { Queue } from '/stereor/Queue'
import { run } from '/stereor/server/run'
import { App } from '/components/App'

Meteor.startup(() => {

	try {
		Lib.systemInitialize()
		run(App)

		startObservers()

		Queue.run()
	} catch (e) {
		console.error(e)
		console.log('oops')
	}

})

